function A = Simulated_Annealing(n, k, params, weights, max_iters, tol, show_plots)
% --- INPUTS:
% n         - size of each community
% k         - number of communities
% params    - desired network parameters, of the form
%             [c_in, c_out, k_in, k_mix, k_out] (1-by-5)
% weights   - weighting for each parameter above. Higher weight => more
%             important (5-by-1)
%
% --- OUTPUTS:
% A         - adjacency matrix of final network

% params = [5, 1, 0.2, 0.01, 0.01];
% weights = [5, 5, 100, 50, 50]';
% n = 100;
% k = 2;
% max_iters = 10000;

% Probability of connection within community
p_in = params(1)/(n*k);
% Probability of connection accros communities
p_out = params(2)/(n*k);
% To track the number of iterations
iters = 1;

% Initial temperature
T_init = 1;
T = T_init;

% Generate the initial SBM network
A = ER_Adj_Communities(n, k, p_in, p_out);
classes = [ones(n,1); 2*ones(n,1)];
e = Energy(A, classes, params, weights);
eList = zeros(max_iters,1);

% For community colour-coding
if show_plots
    nodeColor = [classes==1, zeros(n*k,1), classes==2];
end

% Perform the simulated annealing until we reach the max number of
% iterations or the energy drops below the tolerance threshold.
while iters < max_iters && e > tol
    % Generate neighbour candidate by randomly creating/destroying edge.
    % Pick two nodes, and if they have an edge destroy it, otherwise create
    % it.
    i = ceil(rand(1)*n*k);
    j = ceil(rand(1)*n*k);
    while j == i    % Avoid making self-loops
        j = ceil(rand(1)*n*k);
    end
    Anew = A;
    Anew(i,j) = xor(Anew(i,j),1);
    Anew(j,i) = Anew(i,j);
    
    % Check the energy of the neighbour, and determine whether we
    % transition
    eprime = Energy(Anew, classes, params, weights);
    eList(iters) = eprime;
    p = Accept_Prob(e,eprime,T);
    
    if p > rand(1) % Accept the transition
        A = Anew;
        e = eprime;
        if show_plots
            plot(graph(A),'Nodecolor',nodeColor,'Nodelabel',[]);
            title(['Iteration ' num2str(iters) ', Energy ' num2str(e) ', Temp ' num2str(T) ', Accept Prob ' num2str(p)])
            pause(0.01);
        end
    end
    
    T = T_init/iters^2;
    iters = iters + 1;
end

% Plot final state and show final parameters
if show_plots
    plot(graph(A),'layout','force','Nodecolor',nodeColor,'Nodelabel',[]);
    [c_in, c_out, k_in, k_mix, k_out] = calculate_Parameters(A, classes)
    figure;
    plot(eList)
    title('Energy over Time')
end