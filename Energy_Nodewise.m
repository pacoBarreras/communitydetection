function e = Energy_Nodewise(A, classes, params, weights)
    % Given an adjacency matrix and community memberships, outputs the energy
    % of the network based on desired parameters params (1x5 vector).
    % Weights is a vector of weights, describing the relative importance of
    % each parameter (higher weight means we care more that it's close to
    % the desired value); should be 5x1.
    [c_in, c_out, k_in, k_mix, k_out] = calculate_Parameters_Nodewise(A, classes);
    
    % Calculate energy as squared distance between current and desired
    % parameters (also penalize empty rows, i.e. disconnected nodes).
    n = size(A,1);
    e = ones(1,n)*([c_in, c_out, k_in, k_mix, k_out] - ones(n,1)*params).^2*weights/n + 1000*sum(sum(A) == 0);
end