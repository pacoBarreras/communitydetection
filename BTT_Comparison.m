% 1. Import network, calculate parameters
% 2. Calculate NBT & BTT
% 3. Classify, measure performance

close all;

% % Get all the files in the random_matrices folder
% data = dir('random_matrices');
num_matrices = 100;
networkDir = 'data/SBM_Networks_At_Threshold';
% files = strings(num_matrices,1);
% 
% for f = 3:(num_matrices + 3)
%     files(f - 2) = strcat('random_matrices/',data(f).name);
% end

% Values of alpha to try
alpha_min = 1;
alpha_inc = 1;
alpha_max = 20;
num_alpha = (alpha_max - alpha_min)/alpha_inc + 1;

c_in = zeros(num_matrices,1);
c_out = zeros(num_matrices,1);
k_in = zeros(num_matrices,1);
k_mix = zeros(num_matrices,1);
k_out = zeros(num_matrices,1);
acc_NBT = zeros(num_matrices,1);
acc_Lap = zeros(num_matrices,1);
acc_BTT = zeros(num_matrices,num_alpha);
third_moment_NBT = zeros(num_matrices, 1);
third_moment_BTT = zeros(num_matrices, num_alpha);

% For eigenvalue validation
conv_NBT = ones(num_matrices,1);
conv_BTT = ones(num_matrices,num_alpha);

% For allowing more iterations of eigenvalue calculation, to ensure
% convergence
opts.maxit = 5000;

for t = 1:num_matrices
    % Import the network
    % [A, classes] = import_Network(files(t));
    A = csvread([networkDir '/Network_' num2str(t) '.txt']);
    n = size(A,1);
    A = sparse((A > 0).*ones(n,n));
    A = A - diag(diag(A));
    classes = csvread([networkDir '/Classes_' num2str(t) '.txt']);
    
    % Make a graph object from A
    G = digraph(A);
    % Get the edges. Column 1 is source node, 2 is target node, 3 is weight.
    E = table2array(G.Edges);
    m = size(E,1);
    
    % Calculate the parameters
    [c_in(t), c_out(t), k_in(t), k_mix(t), k_out(t)] = calculate_Parameters(A, classes);
    
    %%------------------------%%
    %           NBT            %
    %%------------------------%% 
    % Find NBT (final digit 0=unnormalized, 1=normalized, i.e., flow matrix)
    NBT = makeNBT(A, 1, 1);
    
    % Classify & measure the accuracy
    [acc_NBT(t), ~] = classify(A, NBT, classes);
    
    % Calculate third moment
    % third_moment_NBT(t) = 1/m*trace(NBT^3); %mean(NBTval.^3);
    
    %%------------------------%%
    %        Laplacian         %
    %%------------------------%%
    L = diag(sum(A,1)) - A;
    [Lapvec, Lapval] = eigs(L, 2, 'LM');
    
    class_Lap = (Lapvec(:,2) > 0) + 1;
    acc_Lap(t) = mean(classes == class_Lap);
    
    if acc_Lap(t) < 0.5
            acc_Lap(t) = 1 - acc_Lap(t);
    end
    
    %%------------------------%%
    %           BTT            %
    %%------------------------%%
    % Try different values of alpha
    parfor a = 1:num_alpha
        alpha = (a - 1)*alpha_inc + alpha_min;
        
        % Find BTT weighing by neighbours in common
        BTT = makeBTT(A, 'nic', alpha, 1);
        
        % Classify & measure the accuracy
        [acc_BTT(1,t,a), ~] = classify(A, BTT, classes);
        
        % Calculate third moment
        % third_moment_BTT(t,a) = 1/m*trace(BTT^3);%mean(BTTval.^3);
    end
    
    disp(t)
end

c_diff = c_in - c_out;
c_threshold = 2*sqrt((c_in + c_out)/2);
% diff = acc_BTT - acc_NBT;
% below_diff = diff(c_diff < threshold);
% sum(below_diff > 0)
% sum(below_diff < 0)
% mean(below_diff(below_diff > 0))
% mean(below_diff(below_diff < 0))

mean_NBT = mean(acc_NBT);
% Make 95% coonfidence estimates
acc_NBT_max = maxk(acc_NBT,ceil(num_matrices*0.05));
acc_NBT_min = mink(acc_NBT,ceil(num_matrices*0.05));

mean_Lap = mean(acc_Lap);

mean_BTT = mean(acc_BTT,1);
% Make 95% coonfidence estimates
acc_BTT_max = maxk(acc_BTT,ceil(num_matrices*0.05));
acc_BTT_min = mink(acc_BTT,ceil(num_matrices*0.05));

[mB, mBi] = max(acc_BTT, [], 2);

% Plot accuracy with 95% confidence intervals
figure(1);
clf;
hold on
    plot(alpha_min:alpha_inc:alpha_max,mean_NBT*ones(num_alpha,1),'-r')
    plot(alpha_min:alpha_inc:alpha_max,mean_Lap*ones(num_alpha,1),'-g')
    plot(alpha_min:alpha_inc:alpha_max,mean_BTT,'-b')
    plot(alpha_min:alpha_inc:alpha_max,acc_BTT_max(end,:),'--b')
    plot(alpha_min:alpha_inc:alpha_max,acc_BTT_min(end,:),'--b')
    plot(alpha_min:alpha_inc:alpha_max,acc_NBT_max(end,:)*ones(num_alpha,1),'--r')
    plot(alpha_min:alpha_inc:alpha_max,acc_NBT_min(end,:)*ones(num_alpha,1),'--r')
hold off
xlim([alpha_min alpha_max])
xlabel('\alpha')
ylabel('Accuracy')
legend('NBT','Laplacian','BTT(\alpha)','Location','best')
title(['Accuracy & 95% Confidence with varying \alpha, ' num2str(num_matrices) ' networks'])

% Plot accuracy showing 1 standard deviation
std_BTT = std(acc_BTT);
std_NBT = std(acc_NBT);
figure(2);
clf;
hold on
    plot(alpha_min:alpha_inc:alpha_max,mean_NBT*ones(num_alpha,1),'-r')
    plot(alpha_min:alpha_inc:alpha_max,mean_Lap*ones(num_alpha,1),'-g')
    plot(alpha_min:alpha_inc:alpha_max,mean_BTT,'-b')
    plot(alpha_min:alpha_inc:alpha_max,mean_BTT + std_BTT,'--b')
    plot(alpha_min:alpha_inc:alpha_max,mean_BTT - std_BTT,'--b')
    plot(alpha_min:alpha_inc:alpha_max,(mean_NBT + std_NBT)*ones(num_alpha,1),'--r')
    plot(alpha_min:alpha_inc:alpha_max,(mean_NBT - std_NBT)*ones(num_alpha,1),'--r')
hold off
xlim([alpha_min alpha_max])
xlabel('\alpha')
ylabel('Accuracy')
legend('NBT','Laplacian','BTT(\alpha)','Location','best')
title(['Accuracy & Std. Dev. with varying \alpha, ' num2str(num_matrices) ' networks'])

% % Plot the network
% colors = [(classes == 1), zeros(n,1),(classes == 2)];
% markers = char((classes == 1)*'o' + (classes == 2)*'x');
% 
% figure(2);
% p = plot(graph(A),'layout','force','Nodelabel',[],'Nodecolor',colors);
% %p.highlight(find(class_BTT ~= classes), 'Marker','x','Markersize',10);

% % For plotting eigenvalues
% alpha = (mBi(end) - 1)*alpha_inc + alpha_min;
% BTT = makeNBT(A, alpha, 1);
% [BTTvec, BTTval] = eigs(BTT, 50, 'LM', opts);
% BTTval = diag(BTTval);
% 
% figure(3);
% clf;
% hold on
%     scatter(real(NBTval),imag(NBTval),'.r')
%     scatter(real(BTTval),imag(BTTval),'.b')
% hold off
% legend('NBT','BTT','Location','best')
% title(['Example of eigenvalues, \alpha = ' num2str(alpha)])

% % Plot the progression of third moments
% col = hsv(num_matrices);
% 
% figure(3)
% clf;
% hold on
% for t = 1:num_matrices
%     %plot(alpha_min:alpha_inc:alpha_max,third_moment_NBT(t)*ones(num_alpha,1),'Color',col(t,:,:,:),'Linewidth',2,'Linestyle','--')
%     plot(alpha_min:alpha_inc:alpha_max,third_moment_BTT(t,:),'Color',col(t,:,:,:))
% end
% hold off
% title('Third moments on 10 networks')
% xlabel('\alpha')
% ylabel('1/2m tr(X^3)')