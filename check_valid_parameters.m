function ret = check_valid_parameters(params)
    ret = true;

    % Get parameters
    c_in = params(1);
    c_out = params(2);
    k_in = params(3);
    k_mix = params(4);
    k_out = params(5);
    
    % Positivity checks
    if c_in < 0 || c_out < 0 || k_in < 0 || k_mix < 0 || k_out < 0
        ret = false;
    % Transitivity equation checks
    elseif k_in > 0 && c_in < 2
        ret = false;
    elseif k_out > 0 && c_out < 2
        ret = false;
    % Degree equation checks
    elseif c_in > 0 && k_in*(c_in - 2) + k_mix*c_out > 2
        ret = false;
    elseif c_out > 0 && k_out*(c_out - 2) + k_mix*c_in > 2
        ret = false;
    end
end