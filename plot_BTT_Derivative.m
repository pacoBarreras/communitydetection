videoName = 'BTT_Spectra';
workingDir = ['Plots/video'];

if ~exist(workingDir, 'dir')
  mkdir(workingDir);
end


outputVideo = VideoWriter(fullfile(workingDir,[videoName '.avi']));
outputVideo.FrameRate = 1;
open(outputVideo)


for a = 1:num_alpha
    alpha = (a - 1)*alpha_inc + alpha_min;
    %BTT_Derivative(A, classes, alpha, 'derivative')
    title(['\alpha = ' num2str(alpha) ', Accuracy = ' num2str(acc_BTT(1,1,a))])
    
    print([workingDir '/image'],'-djpeg90')
    img = imread(fullfile(workingDir,'image.jpg'));
    writeVideo(outputVideo,img);
    pause(0.01);
end

% Removes image.jpg, since it is no longer needed
delete([workingDir '/image.jpg']);
% Finish making the video by closing the output stream
close(outputVideo);
