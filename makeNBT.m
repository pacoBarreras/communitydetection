function [NBT] = makeNBT(A, alpha, normalize)
% Given an adjacency matrix A, returns the non-backtracking matrix NBT.
% --- INPUTS:
%   A           - adjacency matrix of (possibly directed) network
%   alpha       - coefficient related to triangle seeking
%   normalize   - whether to normalize the matrix to be row-stochastic
%
% --- OUTPUT:
%   NBT         - non-backtracking matrix, with triangles weighted by alpha

% Make a graph object from A
G = digraph(A);
% Get the edges. Column 1 is source node, 2 is target node, 3 is weight.
E = table2array(G.Edges);

m = size(E,1);

NBT = zeros(m,m);

for i = 1:m
    for j = 1:m
        % Check if tail of ith edge matches head of jth edge, AND we're not backtracking
        if E(i,2) == E(j,1) && E(j,2) ~= E(i,1)
            if A(E(j,2),E(i,1)) > 0 % Check if there's an edge from tail of j to head of i
            % if ismember([E(j,2), E(i,1),  1], E, 'rows') ----- OLD SLOW METHOD
                NBT(i,j) = alpha;
            else
                NBT(i,j) = 1;
            end
        end
    end
end

if normalize == 1
    rowSum = sum(NBT,2);
    % Ensure that if there are any empty rows, we don't divide by 0.
    rowSum(rowSum == 0) = -1;

    % Normalize the NBT
    NBT = NBT./rowSum;
end

NBT = sparse(NBT);
% x = 0:0.01:2*pi;
% 
% figure;
% clf
% hold on
%     plot(cos(x), sin(x), '-b');                   % Plot the unit circle
%     plot([0 1], [0 0], '-b');                     % Plot line at 0 rad
%     plot([0 cos(2*pi/3)], [0 sin(2*pi/3)], '-b')  % Plot line at 2pi/3 rad
%     plot([0 cos(4*pi/3)], [0 sin(4*pi/3)], '-b')  % Plot line at 4pi/3 rad
%     scatter(real(ev), imag(ev),'.r');             % Plot the eigenvalues
% hold off
% title('Eigenvalues of NBT Matrix')
% xlabel('Real Component')
% ylabel('Imaginary Component')