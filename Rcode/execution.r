#Author: Francisco Barreras
#Date: Jan 17 2018
# Functions to generate random matrices with modified stochastic block model.
###############################
#Load packages
packs= c("compiler",
         "data.table",
         "expm",
         "ggplot2",
         "igraph",
         "matlib",
         "Matrix",
         "statnet")
lapply(packs, require, character.only = TRUE)
enableJIT(3)#Compile functions for extra speed



