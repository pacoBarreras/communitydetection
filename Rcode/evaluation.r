#' nmi
#' compute the Normalized Mutual Information of two sets of labels.
#' @param a first set of labels
#' @param b second set of labels
#'
#' @return a number between 0 and 1 proportional to the mutual information of the two labelings
#' where 0 means that the two are completely independent and 1 means they are equivalent.
#' @export NULL
nmi <- function(a,b){
  C = table(a,b) #Generate a contingency table
  numerator = -2*sum(mapply(function(i,j) C[i,j]*log(C[i,j]*sum(C)/(sum(C[i,])*sum(C[,j]))),
                            i = expand.grid(rownames(C),colnames(C))[,1],
                            j=expand.grid(rownames(C),colnames(C))[,2]),na.rm = T)
  denominator = sum(sapply(rownames(C),function(i) sum(C[i,])*log(sum(C[i,])/sum(C))),na.rm = T) + sum(sapply(colnames(C),function(j) sum(C[,j])*log(sum(C[,j])/sum(C))),na.rm = T)
  numerator/denominator
}

nbt_matrix = function(A,visualize=F){
  n = nrow(A)
  m <- sum(A)               # m: number of directed edges
  X <- A         # X: still the adjacency matrix,
  X[which(X!=0)] <- 1:m       #    but edges are numbered
  #Naming the edges
  p <- which(X!=0)-1
  edgeNames <- apply(matrix(c((p %% n)+1,p %/% n+1),m),1,
                     function(v){paste(v,collapse=".")})      # names of the edges
  
  A.line <- Matrix(0,m,m)     # A.line will become the adjacency matrix of the line graph
  rownames(A.line) <- edgeNames
  colnames(A.line) <- edgeNames
  #We populate matrix A.line minding the non-backtracking 
  for(i in 1:n){ #We pass along the rows of matrix X
    p <- which(X[i,]!=0) #which columns contain the non-zero entries
    if(length(p)>0){
      first_legs = X[i,p] #the first leg edges...
      second_legs = apply(X[p,-i,drop=F],1,function(x) if(nnzero(x)>0){
        return(x[which(x!=0)])}else{
          return(NA)}) #the -i removes self-backtracking
      #Positions of places to put a 1
      pos= unlist(mapply(function(X,Y,dime) X + dime*(Y-1),
                         X = first_legs,
                         Y= second_legs,
                         dime=m))
      #we remove NAs
      pos = pos[!is.na(pos)]
      A.line[pos] = 1 }
  }
  if(visualize){
    spectrum = eigen(A.line,only.values = T)$values
    spectrum.df = data.frame(Re = Re(spectrum),Im = Im(spectrum), type = c("1st","2nd",rep("other",length(spectrum)-2)))
    
    P= ggplot(data = spectrum.df,aes(x=Re,y=Im)) +
      geom_point(mapping=aes(colour= type),show.legend = F,size=0.7)+
      geom_path(data = circleFun(center = c(0,0),
                                 diameter = 2.07*quantile(abs(spectrum),0.99),
                                 npoints = 200),
                mapping=aes(x = x, y = y),
                color="red",
                size=0.2,
                linetype="dashed") + 
      ggtitle("NBT Matrix Spectrum",subtitle = paste0("N = ",nrow(A)))
    plot(P)}
  return(A.line)
}

flow_matrix = function(A,alpha=1,normalized=T,visualize=F){
  n = nrow(A)
  m <- sum(A)               # m: number of directed edges
  X <- A         # X: still the adjacency matrix,
  X[which(X!=0)] <- 1:m       #    but edges are numbered
  #Naming the edges
  p <- which(X!=0)-1
  edgeNames <- apply(matrix(c((p %% n)+1,p %/% n+1),m),1,
                     function(v){paste(v,collapse=".")})      # names of the edges
  
  A.flow <- Matrix(0,m,m)     # A.line will become the adjacency matrix of the line graph
  rownames(A.flow) <- edgeNames
  colnames(A.flow) <- edgeNames
  #We populate matrix A.line minding the non-backtracking 
  for(i in 1:n){ #We pass along the rows of matrix X
    p <- which(X[i,]!=0) #which columns contain the non-zero entries = j nodes
    if(length(p)>0){
      first_legs = X[i,p] #the first leg edges...
      second_legs = apply(X[p,,drop=F],
                          1,
                          function(x) if(nnzero(x)>0){ k_verts <- setdiff(which(x!=0),i);
                          is.p = 2*((X[k_verts,i] > 0) - 0.5); #+1 is in a triangle -1 is a wedge
                          return(x[k_verts]*is.p)}else{
                            return(NA)}) # We remove edges pointing to i, backtrackinga
      #Positions of places to add entries
      if(length(second_legs)>0){pos= unlist(mapply(function(X,Y,dime) X + dime*(abs(Y)-1),
                         X = first_legs,
                         Y= second_legs,
                         dime=m))
      #We construct the value that will be in each position
      values = unlist(lapply(X=second_legs, FUN=function(x){
        (ifelse(sign(x)==1, 1 , 1/alpha)) 
      }))
      #we remove NAs
      pos = pos[!is.na(pos)]
      values = values[!is.na(values)]
      A.flow[pos] = values}
    }}
  if(normalized){
    temp_dimnames= dimnames(A.flow)
    A.flow = diag(1/rowSums(A.flow)) %*% A.flow
    dimnames(A.flow) = temp_dimnames}
  if(visualize){
    spectrum = spectrum = RSpectra::eigs(A.flow,n,which="LM",opts= list(maxitr=2500,retvec = FALSE))$values
    spectrum.df = data.frame(Re = Re(spectrum),Im = Im(spectrum), type = c("1st","2nd",rep("other",length(spectrum)-2)))
    
    P= ggplot(data = spectrum.df,aes(x=Re,y=Im)) +
      geom_point(mapping=aes(colour= type),show.legend = F)+
      geom_path(data = circleFun(center = c(0,0),
                                 diameter = 2.07*quantile(abs(spectrum),0.99),
                                 npoints = 200),
                mapping=aes(x = x, y = y),
                color="red",
                size=0.2,
                linetype="dashed") + 
      ggtitle("Weighted Flow Matrix Spectrum",subtitle = paste0("N = ",n,", alph = ", alpha,", normalized = ",normalized))
    plot(P)}
  return(A.flow)
}

flow_matrix_deg = function(A,alpha=1,normalized=T,visualize=F){
  n = nrow(A)
  m <- sum(A)               # m: number of directed edges
  X <- A         # X: still the adjacency matrix,
  X[which(X!=0)] <- 1:m       #    but edges are numbered
  #Naming the edges
  p <- which(X!=0)-1
  edgeNames <- apply(matrix(c((p %% n)+1,p %/% n+1),m),1,
                     function(v){paste(v,collapse=".")})      # names of the edges
  
  A.flow <- Matrix(0,m,m)     # A.line will become the adjacency matrix of the line graph
  rownames(A.flow) <- edgeNames
  colnames(A.flow) <- edgeNames
  #We populate matrix A.line minding the non-backtracking 
  for(i in 1:n){ #We pass along the rows of matrix X
    deg.i = nnzero(X[10,])
    p <- which(X[i,]!=0) #which columns contain the non-zero entries = j nodes
    if(length(p)>0){
      first_legs = X[i,p] #the first leg edges...
      second_legs = apply(X[p,,drop=F],
                          1,
                          function(x) if(nnzero(x)>0){ k_verts <- setdiff(which(x!=0),i);
                          is.p = 2*((X[k_verts,i] > 0) - 0.5); #+1 is in a triangle -1 is a wedge
                          return(x[k_verts]*is.p)}else{
                            return(NA)}) # We remove edges pointing to i, backtracking
      #Positions of places to add entries
      if(length(second_legs)>0){pos= unlist(mapply(function(X,Y,dime) X + dime*(abs(Y)-1),
                                                   X = first_legs,
                                                   Y= second_legs,
                                                   dime=m))
      #We construct the value that will be in each position
      values = unlist(lapply(X=second_legs, FUN=function(x){
        ifelse(sign(x)==1, 1 , 1/(deg.i*alpha))})) }
      #we remove NAs
      pos = pos[!is.na(pos)]
      values = values[!is.na(values)]
      A.flow[pos] = values}}
  if(normalized){
    temp_dimnames= dimnames(A.flow)
    A.flow = diag(1/rowSums(A.flow)) %*% A.flow
    dimnames(A.flow) = temp_dimnames}
  if(visualize){
    spectrum = spectrum = RSpectra::eigs(A.flow,n,which="LM",opts= list(maxitr=2500,retvec = FALSE))$values
    spectrum.df = data.frame(Re = Re(spectrum),Im = Im(spectrum), type = c("1st","2nd",rep("other",length(spectrum)-2)))
    
    P= ggplot(data = spectrum.df,aes(x=Re,y=Im)) +
      geom_point(mapping=aes(colour= type),show.legend = F)+
      geom_path(data = circleFun(center = c(0,0),
                                 diameter = 2.07*quantile(abs(spectrum),0.99),
                                 npoints = 200),
                mapping=aes(x = x, y = y),
                color="red",
                size=0.2,
                linetype="dashed") + 
      ggtitle("Weighted Flow Matrix Spectrum",subtitle = paste0("N = ",n,", alph = ", alpha,", normalized = ",normalized))
    plot(P)}
  return(A.flow)
}

flow_matrix_nic = function(A,alpha=1,normalized=T,visualize=F){
  n = nrow(A)
  m <- sum(A)               # m: number of directed edges
  X <- A         # X: still the adjacency matrix,
  X[which(X!=0)] <- 1:m       #    but edges are numbered
  #Naming the edges
  p <- which(X!=0)-1
  edgeNames <- apply(matrix(c((p %% n)+1,p %/% n+1),m),1,
                     function(v){paste(v,collapse=".")})      # names of the edges
  
  A.flow <- Matrix(0,m,m)     # A.line will become the adjacency matrix of the line graph
  rownames(A.flow) <- edgeNames
  colnames(A.flow) <- edgeNames
  #We populate matrix A.line minding the non-backtracking 
  for(i in 1:n){ #We pass along the rows of matrix X
     p <- which(X[i,]!=0) #which columns contain the non-zero entries = j nodes
    if(length(p)>0){
      first_legs = X[i,p] #the first leg edges...
      second_legs = apply(X[p,,drop=F],
                          1,
                          function(x) if(nnzero(x)>0){ k_verts <- setdiff(which(x!=0),i);
                          is.p = 2*((X[k_verts,i] > 0) - 0.5); #+1 is in a triangle -1 is a wedge
                          return(x[k_verts]*is.p)}else{
                            return(NA)}) # We remove edges pointing to i, backtracking
      #Positions of places to add entries
      if(length(second_legs)>0){pos= unlist(mapply(function(X,Y,dime) X + dime*(abs(Y)-1),
                                                   X = first_legs,
                                                   Y= second_legs,
                                                   dime=m))
      #We construct the value that will be in each position
      values = unlist(lapply(X=second_legs, FUN=function(x){
        ifelse(sign(x)==1, 1 , 1/((sum(x>0)+1)*alpha) )})) }
      #we remove NAs
      pos = pos[!is.na(pos)]
      values = values[!is.na(values)]
      A.flow[pos] = values}}
  if(normalized){
    temp_dimnames= dimnames(A.flow)
    A.flow = diag(1/rowSums(A.flow)) %*% A.flow
    dimnames(A.flow) = temp_dimnames}
  if(visualize){
    spectrum = spectrum = RSpectra::eigs(A.flow,n,which="LM",opts= list(maxitr=2500,retvec = FALSE))$values
    spectrum.df = data.frame(Re = Re(spectrum),Im = Im(spectrum), type = c("1st","2nd",rep("other",length(spectrum)-2)))
    
    P= ggplot(data = spectrum.df,aes(x=Re,y=Im)) +
      geom_point(mapping=aes(colour= type),show.legend = F)+
      geom_path(data = circleFun(center = c(0,0),
                                 diameter = 2.07*quantile(abs(spectrum),0.99),
                                 npoints = 200),
                mapping=aes(x = x, y = y),
                color="red",
                size=0.2,
                linetype="dashed") + 
      ggtitle("Weighted Flow Matrix Spectrum",subtitle = paste0("N = ",n,", alph = ", alpha,", normalized = ",normalized))
    plot(P)}
  return(A.flow)
}

flow_matrix_tri = function(A,alpha=1,normalized=T,visualize=F){
  n = nrow(A)
  m <- sum(A)               # m: number of directed edges
  X <- A         # X: still the adjacency matrix,
  X[which(X!=0)] <- 1:m       #    but edges are numbered
  #Naming the edges
  p <- which(X!=0)-1
  edgeNames <- apply(matrix(c((p %% n)+1,p %/% n+1),m),1,
                     function(v){paste(v,collapse=".")})      # names of the edges
  
  A.flow <- Matrix(0,m,m)     # A.line will become the adjacency matrix of the line graph
  rownames(A.flow) <- edgeNames
  colnames(A.flow) <- edgeNames
  #We populate matrix A.line minding the non-backtracking 
  for(i in 1:n){ #We pass along the rows of matrix X
    p <- which(X[i,]!=0) #which columns contain the non-zero entries = j nodes
    tri.i = sum(sapply(1:length(p),function(z) length(p[p %in% which(X[p[z],-i]!=0)])>0,USE.NAMES = F))
    if(length(p)>0){
      first_legs = X[i,p] #the first leg edges...
      second_legs = apply(X[p,,drop=F],
                          1,
                          function(x) if(nnzero(x)>0){ k_verts <- setdiff(which(x!=0),i);
                          is.p = 2*((X[k_verts,i] > 0) - 0.5); #+1 is in a triangle -1 is a wedge
                          return(x[k_verts]*is.p)}else{
                            return(NA)}) # We remove edges pointing to i, backtracking
      #Positions of places to add entries
      if(length(second_legs)>0){pos= unlist(mapply(function(X,Y,dime) X + dime*(abs(Y)-1),
                                                   X = first_legs,
                                                   Y= second_legs,
                                                   dime=m))
      #We construct the value that will be in each position
      values = unlist(lapply(X=second_legs, FUN=function(x){
        ifelse(sign(x)==1, 1 , 1/((tri.i+1)*alpha))})) }
      #we remove NAs
      pos = pos[!is.na(pos)]
      values = values[!is.na(values)]
      A.flow[pos] = values}}
  if(normalized){
    temp_dimnames= dimnames(A.flow)
    A.flow = diag(1/rowSums(A.flow)) %*% A.flow
    dimnames(A.flow) = temp_dimnames}
  if(visualize){
    spectrum = spectrum = RSpectra::eigs(A.flow,n,which="LM",opts= list(maxitr=2500,retvec = FALSE))$values
    spectrum.df = data.frame(Re = Re(spectrum),Im = Im(spectrum), type = c("1st","2nd",rep("other",length(spectrum)-2)))
    
    P= ggplot(data = spectrum.df,aes(x=Re,y=Im)) +
      geom_point(mapping=aes(colour= type),show.legend = F)+
      geom_path(data = circleFun(center = c(0,0),
                                 diameter = 2.07*quantile(abs(spectrum),0.99),
                                 npoints = 200),
                mapping=aes(x = x, y = y),
                color="red",
                size=0.2,
                linetype="dashed") + 
      ggtitle("Weighted Flow Matrix Spectrum",subtitle = paste0("N = ",n,", alph = ", alpha,", normalized = ",normalized))
    plot(P)}
  return(A.flow)
}

flow_matrix_tm = function(A,alpha=1,normalized=T,visualize=F){
  n = nrow(A)
  m <- sum(A)               # m: number of directed edges
  X <- A         # X: still the adjacency matrix,
  X[which(X!=0)] <- 1:m       #    but edges are numbered
  #Naming the edges
  p <- which(X!=0)-1
  edgeNames <- apply(matrix(c((p %% n)+1,p %/% n+1),m),1,
                     function(v){paste(v,collapse=".")})      # names of the edges
  
  A.flow <- Matrix(0,m,m)     # A.line will become the adjacency matrix of the line graph
  rownames(A.flow) <- edgeNames
  colnames(A.flow) <- edgeNames
  #We populate matrix A.line minding the non-backtracking 
  for(i in 1:n){ #We pass along the rows of matrix X
    p <- which(X[i,]!=0) #which columns contain the non-zero entries = j nodes
    tm.i = (A%*%A%*%A)[i,i]
    if(length(p)>0){
      first_legs = X[i,p] #the first leg edges...
      second_legs = apply(X[p,,drop=F],
                          1,
                          function(x) if(nnzero(x)>0){ k_verts <- setdiff(which(x!=0),i);
                          is.p = 2*((X[k_verts,i] > 0) - 0.5); #+1 is in a triangle -1 is a wedge
                          return(x[k_verts]*is.p)}else{
                            return(NA)}) # We remove edges pointing to i, backtracking
      #Positions of places to add entries
      if(length(second_legs)>0){pos= unlist(mapply(function(X,Y,dime) X + dime*(abs(Y)-1),
                                                   X = first_legs,
                                                   Y= second_legs,
                                                   dime=m))
      #We construct the value that will be in each position
      values = unlist(lapply(X=second_legs, FUN=function(x){
        ifelse(sign(x)==1, 1 , 1/((tm.i+1)*alpha))})) }
      #we remove NAs
      pos = pos[!is.na(pos)]
      values = values[!is.na(values)]
      A.flow[pos] = values}}
  if(normalized){
    temp_dimnames= dimnames(A.flow)
    A.flow = diag(1/rowSums(A.flow)) %*% A.flow
    dimnames(A.flow) = temp_dimnames}
  if(visualize){
    spectrum = spectrum = RSpectra::eigs(A.flow,n,which="LM",opts= list(maxitr=2500,retvec = FALSE))$values
    spectrum.df = data.frame(Re = Re(spectrum),Im = Im(spectrum), type = c("1st","2nd",rep("other",length(spectrum)-2)))
    
    P= ggplot(data = spectrum.df,aes(x=Re,y=Im)) +
      geom_point(mapping=aes(colour= type),show.legend = F)+
      geom_path(data = circleFun(center = c(0,0),
                                 diameter = 2.07*quantile(abs(spectrum),0.99),
                                 npoints = 200),
                mapping=aes(x = x, y = y),
                color="red",
                size=0.2,
                linetype="dashed") + 
      ggtitle("Weighted Flow Matrix Spectrum",subtitle = paste0("N = ",n,", alph = ", alpha,", normalized = ",normalized))
    plot(P)}
  return(A.flow)
}

classify = function(red, path_matrix, visualize=T){
  #Extract the eigenvector associated with the second absolute real eigenvalue (might be negative)
  spectrum <- RSpectra::eigs(path_matrix,2,which="LM",opts= list(maxitr=2500))
  community = Re(spectrum$vectors[,2])
  #Now we sum all edges pointing to i and calculate the sign
  #First, which edges are pointing to i:
  points=sapply(strsplit(x = dimnames(path_matrix)[[2]],split = "\\."), function(pair) pair[2])
  #Then we classify:
  predicted = sapply(1:length(V(red)), function(i) sum(community[points == i]))
  true = as.factor(V(red)$class)
  #We make the prediction negative for the first class...
  predicted= -sign(mean(predicted[true==1]))*predicted
  #We plot the histogram of predicted vs true_classes
  #Accuracy computation:
  contingency = data.frame(predicted,true)
  contingency$order = 1:nrow(contingency)
  
  accuracy = sum(diag(table(sign(predicted+1e-19),true)))/length(predicted)
  accuracy = max(accuracy,1-accuracy)
  nmi = nmi(a=true,b=sign(predicted+1e-19))
  
  #Identify confused nodes looking at homophilly
  
  if(visualize){conf_nodes = which(V(red)$homophily < 0)
  conf_nodes.df = data.frame(order=conf_nodes,
                             predicted = predicted[conf_nodes],
                             true = true[conf_nodes])
  
  P=ggplot(contingency,aes(x=order,y=predicted,colour = true)) +
    geom_line(size=0.45,show.legend = F)+
    geom_hline(yintercept = 0,color="red",linetype="dashed",size=0.2)+
    ggtitle("Distribution of Prediction vs. Class",subtitle = paste0("NBT, N= ",length(V(red))))+
    annotate("label", x = quantile(contingency$order,0.85), y = 1, label = paste0("Accuracy: ", round(accuracy,4)))+
    annotate("label", x = quantile(contingency$order,0.85), y = 0.8, label = paste0("NMI: ", round(nmi,4)))+
    geom_point(data = conf_nodes.df,mapping = aes(x= order,y=predicted,colour = true),shape=0,show.legend = F)
  plot(P)}
  return(list(accuracy,nmi))
}

best_alpha = function(net,depth=8,lims = c(1,30)){
  #Perform a ternary search
  low = lims[1]
  high= lims[2]
  for(i in 1:depth){
    m1 = low + (high - low)/3
    m2 = high -(high - low)/3
    acc_m1 = classify(net,flow_matrix(A = as_adj(net) , alpha = m1),F)[[1]]
    acc_m2 = classify(net,flow_matrix(A = as_adj(net) , alpha = m2),F)[[1]]
    if(acc_m1 <= acc_m2){low = m1}else{
      high = m2
    } 
  }
  return( list(alpha=high,acc = acc_m2) )
}

best_alpha_deg = function(net,depth=8,lims = c(1,30)){
  #Perform a ternary search
  low = lims[1]
  high= lims[2]
  for(i in 1:depth){
    m1 = low + (high - low)/3
    m2 = high -(high - low)/3
    acc_m1 = classify(net,flow_matrix_deg(A = as_adj(net) , alpha = m1),F)[[1]]
    acc_m2 = classify(net,flow_matrix_deg(A = as_adj(net) , alpha = m2),F)[[1]]
    if(acc_m1 <= acc_m2){low = m1}else{
      high = m2
    } 
  }
  return( list(alpha=high,acc = acc_m2) )
}

best_alpha_nic = function(net,depth=10,lims = c(1,30)){
  #Perform a ternary search
  low = lims[1]
  high= lims[2]
  for(i in 1:depth){
    m1 = low + (high - low)/3
    m2 = high -(high - low)/3
    acc_m1 = classify(net,flow_matrix_nic(A = as_adj(net) , alpha = m1),F)[[1]]
    acc_m2 = classify(net,flow_matrix_nic(A = as_adj(net) , alpha = m2),F)[[1]]
    if(acc_m1 <= acc_m2){low = m1}else{
      high = m2
    } 
  }
  return( list(alpha=high,acc = acc_m2) )
}
best_alpha_nic = function(net,depth=10,lims = c(1,30)){
  #Perform a ternary search
  low = lims[1]
  high= lims[2]
  for(i in 1:depth){
    m1 = low + (high - low)/3
    m2 = high -(high - low)/3
    acc_m1 = classify(net,flow_matrix_nic(A = as_adj(net) , alpha = m1),F)[[1]]
    acc_m2 = classify(net,flow_matrix_nic(A = as_adj(net) , alpha = m2),F)[[1]]
    if(acc_m1 <= acc_m2){low = m1}else{
      high = m2
    } 
  }
  return( list(alpha=high,acc = acc_m2) )
}

best_alpha_tri = function(net,depth=10,lims = c(1,30)){
  #Perform a ternary search
  low = lims[1]
  high= lims[2]
  for(i in 1:depth){
    m1 = low + (high - low)/3
    m2 = high -(high - low)/3
    acc_m1 = classify(net,flow_matrix_tri(A = as_adj(net) , alpha = m1),F)[[1]]
    acc_m2 = classify(net,flow_matrix_tri(A = as_adj(net) , alpha = m2),F)[[1]]
    if(acc_m1 <= acc_m2){low = m1}else{
      high = m2
    } 
  }
  return( list(alpha=high,acc = acc_m2) )
}

best_alpha_tm = function(net,depth=10,lims = c(1,30)){
  #Perform a ternary search
  low = lims[1]
  high= lims[2]
  for(i in 1:depth){
    m1 = low + (high - low)/3
    m2 = high -(high - low)/3
    acc_m1 = classify(net,flow_matrix_tm(A = as_adj(net) , alpha = m1),F)[[1]]
    acc_m2 = classify(net,flow_matrix_tm(A = as_adj(net) , alpha = m2),F)[[1]]
    if(acc_m1 <= acc_m2){low = m1}else{
      high = m2
    } 
  }
  return( list(alpha=high,acc = acc_m2) )
}
