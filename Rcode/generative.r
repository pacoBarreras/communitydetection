#Author: Francisco Barreras
#Date: Mar 07 2018
# Functions to generate random matrices with modified stochastic block model,
# as well as obtaining useful statistics and visualization.
###############################

#' generate_sbm 
#' generate a random network according to a stochastic block model with
#' homogeneous connectivity profiles.
#' @param n number of nodes in the network, might be rounded down when probs = NULL
#' @param k number of communities
#' @param probs vector of probabilities of belonging to each community.
#'              If NULL then the communities are perfectly balanced
#' @param c_in Probability of connecting two nodes from the same community
#' @param c_out Probability of connecting two nodes from different communities.
#'
#' @return an igraph object with the network + attributes
#' @export
#'
#' @examples
generate_sbm = function(n=100,
                        k=2,
                        probs=NULL,
                        c_in=2.8,
                        c_out=1.2){
  #Assign classes
  if(is.null(probs)){
    n=floor(n/k)*k
    classes= sort(rep(1:k,n/k))
  }else{
    classes= sort(sample(1:k,size = n,prob = probs,replace=T))}
  #Step 1 populate adjacency matrix (above diagonal)
  A = matrix(nrow = n,ncol = n,data=0)
  for(i in 1:(n-1)){
    for(j in (i+1):n){
      A[i,j] = rbinom(1,1, (c_in*(classes[i]==classes[j]) + c_out*(1-(classes[i]==classes[j])))/n)
    }
  }
  #Complete lower entries symmetrically 
  A = A + t(A)
  #Form i.graph object with attributes using the pipe (%>%) operator
  red = graph_from_adjacency_matrix(A, mode = "undirected")
  red=  red %>%
        set_vertex_attr(name="class", value = classes) %>%
        set_graph_attr(name="type",value = "SBM") %>%
        set_graph_attr(name="parameters",value = list(n = n,
                                                      k = k,
                                                      c_in= c_in,
                                                      c_out=c_out,
                                                      t_hom = round(c_in/n,3),
                                                      t_in = round(c_in/n,3),
                                                      t_out = round(c_out/n,3))) %>%
        set_graph_attr(name="avg_deg",value = mean(igraph::degree(red))) %>%
        set_graph_attr(name="transitivity", transitivity(red)) %>%
        set_vertex_attr(name= "homophily",value = round(sapply(V(red),
                                                               function(t) mean(classes[neighbors(red,t)] == classes[t])) - 0.5,3))
  return(red)
}

#' generate_tc_sbm 
#' generate a random network according to a transitively closed stochastic block
#' model, where a second stage of wedge closing is performed with probabilities
#' given by the kappa parameters.
#' @param n number of nodes in the network, might be rounded down when probs = NULL
#' @param k number of communities
#' @param probs vector of probabilities of belonging to each community.
#'              If NULL then the communities are perfectly balanced
#' @param c_in Probability of connecting two nodes from the same community
#' @param c_out Probability of connecting two nodes from different communities.
#' @param kappa_hom Probability of connecting two nodes in the second stage
#'                  when they are in a homogeneous wedge.
#' @param kappa_in Probability of connecting two nodes in the second stage
#'                  when they are in a heterogeneous wedge but are in the same class.
#' @param kappa_out Probability of connecting two nodes in the second stage
#'                  when they are in a heterogeneous wedge but are in different classes.
#'
#' @return an igraph object with the network + attributes
#' @export
#'
#' @examples
generate_tc_sbm = function(n=100,
                            k=2,
                            probs=NULL,
                            c_in= 6,
                            c_out= 4,
                            t_hom = 0.2,
                            t_in = 0.2,
                            t_out = 0.2){
  #Assign classes
  if(is.null(probs)){
    n=floor(n/k)*k
    classes= sort(rep(1:k,n/k))
  }else{
    classes= sort(sample(1:k,size = n,prob = probs,replace=T))}
  #Step 1 populate adjacency matrix (above diagonal) according to SBM
  A = matrix(nrow = n,ncol = n,data=0)
  for(i in 1:(n-1)){
    for(j in (i+1):n){
      A[i,j] = rbinom(1,1, (c_in*(classes[i]==classes[j]) + c_out*(1-(classes[i]==classes[j])))/n)
    }
  }
  #We make A symmetric
  A = A + t(A)
  #Now we find all pairs of nodes that could be connected on the second stage:
  pairs = which((A%*%A - diag(x = rowSums(A),nrow = n,ncol = n)) != 0,arr.ind = T)
  count = (A%*%A)[  which((A%*%A - diag(x = rowSums(A),nrow = n,ncol = n)) != 0)  ]
  #with the count of wedges for each pair plus the count of homogeneous wedges
  #we can obtain how many wedges of each type each pair belongs to
  count_homo= sapply(1:nrow(pairs), function(r){
                      common_friends = which(A[pairs[r,1],]*A[pairs[r,2],]>0)
                      sum(classes[common_friends]== classes[pairs[r,1]] & classes[common_friends]== classes[pairs[r,2]])
                    })

 #Now we complete triangles á la Victor by doing a weighted average of
 # the kappa parameters (this is extremely non elegant)
 for(r in 1:nrow(pairs)){
   ni=pairs[r,1]
   nj=pairs[r,2]
    t = (1/count[r]^2)*((classes[ni]!=classes[nj])*(count[r])*t_out +
                          (classes[ni]==classes[nj])*((count_homo[r])*t_hom +
                                                        (count[r]- count_homo[r])*t_in))
    A[ni,nj] = max(A[ni,nj],rbinom(1,1,t))
 }
#Form i.graph object with attributes using the pipe (%>%) operator
red = graph_from_adjacency_matrix(A, mode = "undirected")
red=  red %>%
  set_vertex_attr(name="class", value = classes) %>%
  set_graph_attr(name="type",value = "TSBM_2") %>%
  set_graph_attr(name="parameters",value = list(n = n,
                                                k = k,
                                                c_in= c_in,
                                                c_out=c_out,
                                                t_hom = t_hom,
                                                t_in = t_in,
                                                t_out = t_out)) %>%
  set_graph_attr(name="avg_deg",value = mean(igraph::degree(red))) %>%
  set_graph_attr(name="transitivity", transitivity(red)) %>%
  set_vertex_attr(name= "homophily",value = round(sapply(V(red),
                                                         function(t) mean(classes[neighbors(red,t)] == classes[t])) - 0.5,3))
return(red)
}

get_triad_samples = function(n,c_in,c_out,t_in,t_mix,t_out){
#We define the distributions
  
#How many samples of each
  n=floor(n/k)*k
  classes= sort(rep(1:k,n/k))
}

check_transitivity_constraints = function(c_in,c_out,t_in,t_mix,t_out){
  
}

sample_triads = function(n,k,c_in,c_out,t_in,t_mix,t_out){
  
}

generate_triad_sbm = function(n=100,
                          k=2,
                          c_in= 6,
                          c_out= 6,
                          t_in = 0.2,
                          t_mix = 0.2,
                          t_out = 0.2){
  #Assign classes
    n=floor(n/k)*k
    classes= sort(rep(1:k,n/k))
  #Define distributions F_hom and F_het
  r_in = c_in/n^2
  r_out= c_out/n^2
  k_hom = (n^2)/((c_in + c_out)/2)*(t_hom)
  k_het = (n^2)/((c_in + c_out)/2)*(t_het) #This could be the average of the two
  if(r_in*k_hom > 1){stop("Distribution F_hom is unfeasible, try lowering t_hom")}
  if(max(r_in,r_out)*k_het > 1){stop("Distribution F_het is unfeasible, try lowering t_het")}
  #compute solution to linear systems:
  a = r_in - 2*r_in^2 + r_in^3*k_hom
  b = (r_in^2)*(1-r_in*k_hom)
  c = (r_in^3)*(k_hom)
  #now for heterogeneous triads
  a.h = r_out-r_out^2-r_in*r_out*(1-r_out*k_het)
  x.h = r_in - r_in*r_out-r_in*r_out*(1-r_out*k_het)
  y.h = (r_out^2)*(1-r_in*k_het)
  b.h = r_in*r_out*(1-r_out*k_het)
  c.h = r_in*(r_out^2)*k_het
  
  triads = list(c(0,0,0),c(1,0,0),c(0,1,0),c(0,0,1),c(0,1,1),c(1,0,1),c(1,1,0),c(1,1,1))
  hom_probs = c(1-3*a-3*b-c,a,a,a,b,b,b,c)
  #Het probs we start with odd edge and minus odd edge
  het_probs = c(1-2*a.h-x.h-2*b.h-y.h-c.h,x.h,a.h,a.h,y.h,b.h,b.h,c.h)
  A = matrix(nrow = n,ncol = n,data=0)
  #Now we loop over all triads (unordered) and complete A
  #To count unique unordered triads we ask that i < j < k
  for(i in 1:(n-2)){
    for(j in (i+1):(n-1)){
      for(l in (j+1):n){
     if(classes[i]==classes[j] & classes[j]==classes[l]){
       edges = sample(triads,size=1,prob = hom_probs)[[1]]
       A[i,j] = max(A[i,j],edges[1])
       A[j,l] = max(A[j,l],edges[2])
       A[i,l] = max(A[i,l],edges[3])
     }else{
       #Determine the odd edge out, the one with two
       #nodes from same community.
       #Equivalent to finding a permutation that
       #Leaves ij,jk,ki in the right order
       permuted = perm((1-(classes[i] == classes[j]))*(-1)^((classes[j] == classes[l])))
       #Sample (remember first edge is odd one out)
       edges = sample(triads,size=1,prob = het_probs)[[1]]
       A[i,j] = max(A[i,j],edges[permuted[1]])
       A[j,l] = max(A[j,l],edges[permuted[2]])
       A[i,l] = max(A[i,l],edges[permuted[3]])
     }
      }#end of k loop
    }#end of j loop
  }#end of i loop
  #We make A symmetric
  A = A + t(A)
  #Form i.graph object with attributes using the pipe (%>%) operator
  red = graph_from_adjacency_matrix(A, mode = "undirected")
  red=  red %>%
    set_vertex_attr(name="class", value = classes) %>%
    set_graph_attr(name="type",value = "TSBM") %>%
    set_graph_attr(name="parameters",value = list(n = n,
                                                  k = k,
                                                  c_in= c_in,
                                                  c_out=c_out,
                                                  t_hom = k_hom*r_in,
                                                  t_in = k_het*r_in,
                                                  t_out = k_het*r_out)) %>%
    set_graph_attr(name="avg_deg",value = mean(igraph::degree(red))) %>%
    set_graph_attr(name="transitivity", transitivity(red)) %>%
    set_vertex_attr(name= "homophily",value = round(sapply(V(red),
                                                           function(t) mean(classes[neighbors(red,t)] == classes[t])) - 0.5,3))
  return(red)
}

deg.dist = function(n, c_in, c_out, t_hom, t_out){
  t_in = (t_out/(1-t_out))*(c_in/c_out)
  #Now we calculate the means
  d_hom_mean = (t_hom/2)*(c_in/2)^2
  d_in_mean = (t_in/2)*(c_out/2)^2
  d_out_mean = (t_out/(1-t_out))*(c_in*c_out/4)
  s_in_mean = c_in/2 - d_out_mean - 2*d_hom_mean
  s_out_mean = c_out/2 - d_out_mean - 2*d_in_mean
  
  d_hom = rpois(n,d_hom_mean)
  d_in = rpois(n,d_in_mean)
  d_out = rpois(n,d_out_mean)
  #We repeat a few times to eliminate unpairable distributions
  while( abs(2*sum(d_in[1:floor(n/2)]) - sum(d_out[(floor(n/2)+1):n])) > 2||
         abs(2*sum(d_in[(floor(n/2)+1):n]) - sum(d_out[1:floor(n/2)]))> 2){
    d_in = rpois(n,d_in_mean)
    d_out = rpois(n,d_out_mean)
  }
  s_in = rpois(n,s_in_mean)
  s_out = rpois(n,s_out_mean)
  #We repeat a few times to eliminate unpairable distributions
  while( abs(sum(s_out[1:floor(n/2)]) - sum(s_out[(floor(n/2)+1):n])) > 2){
    s_out = rpois(n,s_out_mean)}
  deg_dist.df=data.frame(d_hom = d_hom,
                         d_in = d_in,
                         d_out = d_out,
                         s_in = s_in,
                         s_out = s_out)
  #We eliminate disconnected graphs whenever possible by shuffling
  while(sum(rowSums(deg_dist.df) == 0)>0){
    deg_dist.df$d_hom = deg_dist.df$d_hom[c(sample(1:floor(n/2),floor(n/2)),sample((floor(n/2)+1):n,floor(n/2)))]
    deg_dist.df$d_in = deg_dist.df$d_in[c(sample(1:floor(n/2),floor(n/2)),sample((floor(n/2)+1):n,floor(n/2)))]
    deg_dist.df$d_out = deg_dist.df$d_out[c(sample(1:floor(n/2),floor(n/2)),sample((floor(n/2)+1):n,floor(n/2)))]
    deg_dist.df$s_in = deg_dist.df$s_in[c(sample(1:floor(n/2),floor(n/2)),sample((floor(n/2)+1):n,floor(n/2)))]
    deg_dist.df$s_out = deg_dist.df$s_out[c(sample(1:floor(n/2),floor(n/2)),sample((floor(n/2)+1):n,floor(n/2)))]}
  return(deg_dist.df)}


generate_newman = function(n=100,
                           k=2,
                           probs=NULL,
                           c_in= 6,
                           c_out= 4,
                           t_hom = 0.1,
                           t_out = 0.1,
                           multiedges=F){
  t_in = (t_out/(1-t_out))*(c_in/c_out)
  #We create the classes
  if(is.null(probs)){
    n=floor(n/k)*k
    classes= sort(rep(1:k,n/k))
  }else{
    classes= sort(sample(1:k,size = n,prob = probs,replace=T))}
  #We obtain the data.frame with the degree distribution (discriminated)
  deg_dist.df = deg.dist(n, c_in, c_out, t_hom, t_out)
  #We populate the adjacency matrix
  A = matrix(nrow = n,ncol = n,data=0)
  resample <- function(x, ...) x[sample.int(length(x), ...)]
  #We exhaust all the s_in connections
  #From the first community
  while( nnzero(deg_dist.df$s_in[which(classes==1)]) > 1 ){
    #Pick two nodes from community 1 connect them and update
    pair = sort(resample(which(classes==1 & deg_dist.df$s_in>0),2))
    A[pair[1],pair[2]] = A[pair[1],pair[2]] + 1
    deg_dist.df$s_in[pair] = deg_dist.df$s_in[pair] - 1
  }
  #From the second community
  while( nnzero(deg_dist.df$s_in[which(classes==2)]) > 1 ){
    #Pick two nodes from community 1 connect them and update
    pair = sort(resample(which(classes==2 & deg_dist.df$s_in>0),2))
    A[pair[1],pair[2]] = A[pair[1],pair[2]] + 1
    deg_dist.df$s_in[pair] = deg_dist.df$s_in[pair] - 1
  }
  #Now we complete all the s_out type of links
  while(length(which(classes==1 & deg_dist.df$s_out>0))>0 && length(which(classes==2 & deg_dist.df$s_out>0))>0){
    pair = c(resample(which(classes==1 & deg_dist.df$s_out>0),1),
             resample(which(classes==2 & deg_dist.df$s_out>0),1))
    A[pair[1],pair[2]] = A[pair[1],pair[2]] + 1
    deg_dist.df$s_out[pair] = deg_dist.df$s_out[pair] - 1
  }
  #Now we complete the innner triangles
  #For community 1
  while( nnzero(deg_dist.df$d_hom[which(classes==1)]) > 2){
    triple = sort(resample( which(classes==1 & deg_dist.df$d_hom>0),3))
    A[triple[1],triple[2]] = A[triple[1],triple[2]] + 1
    A[triple[1],triple[3]] = A[triple[1],triple[3]] + 1
    A[triple[2],triple[3]] = A[triple[2],triple[3]] + 1
    deg_dist.df$d_hom[triple] = deg_dist.df$d_hom[triple] - 1}
  #For community 2
  while( nnzero(deg_dist.df$d_hom[which(classes==2)]) > 2){
    triple = sort(resample( which(classes==2 & deg_dist.df$d_hom>0),3))
    A[triple[1],triple[2]] = A[triple[1],triple[2]] + 1
    A[triple[1],triple[3]] = A[triple[1],triple[3]] + 1
    A[triple[2],triple[3]] = A[triple[2],triple[3]] + 1
    deg_dist.df$d_hom[triple] = deg_dist.df$d_hom[triple] - 1}
  #Now we form triangles of form d_in, d_out, d_out
  #for community 1
  while( length(which(deg_dist.df$d_in>0 & classes==1))>0 &&
         length(which(deg_dist.df$d_out>0 & classes==2))>1){
    triple = sort(c(resample(which(deg_dist.df$d_in>0 & classes==1),1),
               resample(which(deg_dist.df$d_out>0 & classes==2),2)))
    A[triple[1],triple[2]] = A[triple[1],triple[2]] + 1
    A[triple[1],triple[3]] = A[triple[1],triple[3]] + 1
    A[triple[2],triple[3]] = A[triple[2],triple[3]] + 1
    deg_dist.df$d_in[triple[1]] = deg_dist.df$d_in[triple[1]] - 1
    deg_dist.df$d_out[triple[-1]] = deg_dist.df$d_out[triple[-1]] - 1}     
  #For community 2
  while( length(which(deg_dist.df$d_in>0 & classes==2))>0 &&
         length(which(deg_dist.df$d_out>0 & classes==1))>1){
    triple = sort(c(resample(which(deg_dist.df$d_in>0 & classes==2),1),
                    resample(which(deg_dist.df$d_out>0 & classes==1),2)))
    A[triple[1],triple[2]] = A[triple[1],triple[2]] + 1
    A[triple[1],triple[3]] = A[triple[1],triple[3]] + 1
    A[triple[2],triple[3]] = A[triple[2],triple[3]] + 1
    deg_dist.df$d_in[triple[3]] = deg_dist.df$d_in[triple[3]] - 1
    deg_dist.df$d_out[triple[-3]] = deg_dist.df$d_out[triple[-3]] - 1}
  #Now we complete network and possibly eliminate multiedges
  #We make A symmetric
  A = A + t(A)
  if(!multiedges){A[A>1] <- 1}
  #Form i.graph object with attributes using the pipe (%>%) operator
  red = graph_from_adjacency_matrix(A, mode = "undirected")
  red=  red %>%
    set_vertex_attr(name="class", value = classes) %>%
    set_graph_attr(name="type",value = "Newman") %>%
    set_graph_attr(name="parameters",value = list(n = n,
                                                  k = k,
                                                  c_in= c_in,
                                                  c_out=c_out,
                                                  t_hom = t_hom,
                                                  t_in = t_in,
                                                  t_out = t_out)) %>%
    set_graph_attr(name="avg_deg",value = mean(igraph::degree(red))) %>%
    set_graph_attr(name="transitivity", transitivity(red)) %>%
    set_vertex_attr(name= "homophily",value = round(sapply(V(red),
                                                           function(t) mean(classes[neighbors(red,t)] == classes[t])) - 0.5,3))
  return(red)
}

edge.weights <- function(classes, network, weight.within = 80, weight.between = 1) {
  el=as_edgelist(network)
  bridges= (classes[el[,1]] == classes[el[,2]])
  
  weights <- ifelse(test = !bridges, yes = weight.between, no = weight.within)
  return(weights) 
}

plot_network = function(net,weight.within=1,weight.between=10,...){
  #We assign weights to be able to plot communities
  weights=edge.weights(classes = V(net)$class,network = net,
                       weight.within = weight.within,weight.between = weight.between)
  #group_layout= layout_with_fr(net,weights = weights)
  group_layout= layout_with_kk(net,
                               weights = weights,
                               epsilon = 0,
                               maxiter = 5000*vcount(net))
  
  #Assign colors
  prettyColors <- c("darkorange", "royalblue","forestgreen", "cyan")
  V(net)$color <- prettyColors[V(net)$class]
  #Plot
  plot(net, mark.groups = NULL,
       layout = group_layout,
       rescale = F,
       vertex.label=NA,...)
}

#Function to plot networks
