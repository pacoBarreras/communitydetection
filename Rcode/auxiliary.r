#' circleFun
#' function to create the coordinates of points in a circle
#' @param center Center of the circle
#' @param diameter Diameter of the circle
#' @param npoints Number of points to be drawn
#'
#' @return a data.frame with the x and y coordinates
#' @export NULL
circleFun <- function(center = c(0,0),diameter = 1, npoints = 100){
  r = diameter / 2
  tt <- seq(0,2*pi,length.out = npoints)
  xx <- center[1] + r * cos(tt)
  yy <- center[2] + r * sin(tt)
  return(data.frame(x = xx, y = yy))
}

#' perm
#' auxiliary function that permutes c(1,2,3)
#' @param sigma 0 is identity, -1 is right shift and 1 is left shift
#'
#' @return the vector c(1,2,3) in permuted order
#' @export NULL
perm = function(sigma){
  if(sigma==0) return(c(1,2,3))
  if(sigma== -1) return(c(3,1,2))
  return(c(2,3,1))
}