function [Adj, classes] = import_Network(filename)
    file = fopen(filename, 'r');    
    % Get the number of nodes, discard the '*Edges' string
    data = textscan(file,'%s %f',2);
    n = data{2}(1);
    
    % Get the edges, and build an adjacency matrix
    data = textscan(file,'%f %f');
    source = data{1};
    target = data{2};
    Adj = sparse(source, target, 1, n, n);
    Adj = Adj + Adj';
    
    % Get the class memberships
    data = textscan(file,'%c');
    
    % Since the nodes are organized by class, we just need to know when
    % they switch from being in class 1 to class 2.
    index = find(data{1} == '2',1) - find(data{1} == '1',1);
    classes = [ones(n - index,1); 2*ones(index,1)];
    
    fclose(file);
end