function C = local_clustering(A)
% OLD (SLOW) METHOD
%     G = graph(A);
%     deg = sum(A,2);
%     n = size(A,1);
%     C = zeros(n,1);
%     for i = 1:n
%         if deg(i) <= 1
%             C(i) = 0;
%         else
%             nbhd = neighbors(G,i);
%             C(i) = 2*numedges(subgraph(G,nbhd))/(deg(i)*(deg(i) - 1));
%         end
%     end
    deg = sum(A,2);
    C = diag(A^3)./( deg.*(deg - 1) ); % Removed factor of 2 from numerator & denominator
    C(isnan(C)) = 0;
    C(isinf(C)) = 0;
end