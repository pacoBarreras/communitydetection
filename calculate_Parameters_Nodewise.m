function [c_in, c_out, k_in, k_mix, k_out] = calculate_Parameters_Nodewise(A, classes)
    % Number of nodes in network
    n = size(A,1);
    
    % Whether a node is in a given class
    class_1 = double(classes == 1);
    class_2 = double(classes == 2);
    
    % Matrix of 1's where the edge goes INSIDE the community
    in = class_1*class_1' + class_2*class_2';
    
    % Matrix of 1's where the edge goes OUTSIDE the community
    out = ones(n,n) - in;
    
    % Average degree inside and across communities
    A_in = A.*in;
    A_out = A.*out;
    c_in = full(sum(A_in,2))*2;
    c_out = full(sum(A_out,2))*2;
    
    % For local clustering inside communities
%     G_in = graph(A_in);
%     nbhd_in = arrayfun(@(x)(neighbors(G_in,x)), 1:n, 'UniformOutput', false); % Neighbours within i's community
%     tri_in = arrayfun(@(x)(numedges(subgraph(G_in,nbhd_in{x}))),1:n); % Number of triangles from i within the community
    d_in = c_in/2;
    tri_in = diag(A_in^3)/2;
    k_in = ( tri_in )./( d_in.*(d_in - 1)/2 );
    k_in(isinf(k_in)) = 0;
    k_in(isnan(k_in)) = 0; % Wherever we divide by zero, set to 0.
    
    % For local clustering outside communities
%     G_out = graph(A_out);
%     nbhd_out = arrayfun(@(x)(neighbors(G_out,x)), 1:n, 'UniformOutput', false); % Neighbours outside i's community
%     tri_out = arrayfun(@(x)(numedges(subgraph(G_in,nbhd_out{x}))),1:n); % Number of triangles from i outside the community
    d_out = c_out/2;
    tri_out = diag(A_out*A_in*A_out)/2;
    k_out = ( tri_out )./( d_out.*(d_out - 1)/2 );
    k_out(isinf(k_out)) = 0;
    k_out(isnan(k_out)) = 0; % Wherever we divide by zero, set to 0.
    
    % For local clustering mixed between communities
%     G = graph(A);
%     nbhd = arrayfun(@(x)(neighbors(G,x)), 1:n, 'UniformOutput', false); % Neighbours of i
%     tri_mix = arrayfun(@(x)(numedges(subgraph(G,nbhd{x}))),1:n) - tri_in - tri_out; % Number of triangles from i mixed between communities
    tri_mix = diag(A^3)/2 - tri_in - tri_out;
    k_mix = ( tri_mix )./( c_in.*c_out/4 );
    k_mix(isnan(k_mix)) = 0; 
    k_mix(isinf(k_mix)) = 0; % Wherever we divide by zero, set to 0.
end