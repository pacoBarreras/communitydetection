function c_avg = count_Third_Moments(A, NBT)
% Size of network
n = size(A,1);
% To count the walks
c_avg = 0;

% Make a graph object from A
G = digraph(A);
% Get the edges. Column 1 is source node, 2 is target node, 3 is weight.
E = table2array(G.Edges);
m = size(E,1);

out = cell(n,1);
inc = cell(n,1);

for i = 1:n
    % Find all outgoing edges
    out{i} = successors(G,i);
    inc{i} = predecessors(G,i);
end

for i = 1:n
    % Loop through nodes from i
    for j = 1:size(out{i},1)
        j_ind = out{i}(j);
        
        % Find nodes k s.t. we have j -> k -> i
        both = intersect(out{j_ind}, inc{i});
        
        % Loop through nodes that complete a closed walk i -> j -> k -> i,
        % and add contributions NBT(i->j, j->k) + NBT(j->k, k->i) + NBT(k->i, i->j)
        for k = 1:size(both,1)
            k_ind = both(k);
            
            % Find indices of edges from the edge list (correspond to NBT)
            ind_ij = find((E(:,1) == i).*(E(:,2) == j_ind));
            ind_jk = find((E(:,1) == j_ind).*(E(:,2) == k_ind));
            ind_ki = find((E(:,1) == k_ind).*(E(:,2) == i));
            
            c_avg = c_avg + NBT(ind_ij, ind_jk) + NBT(ind_jk, ind_ki) + NBT(ind_ki, ind_jk);
        end
    end
end

% Take the average
c_avg = c_avg/n;

end