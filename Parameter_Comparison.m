% Import network, calculate parameters

close all;

num_matrices = 1000;
n = 200;
networkDir = 'SBM_1000_Below_Threshold';

% For saving parameters
c_in = zeros(num_matrices, n);
c_out = zeros(num_matrices, n);
k_in = zeros(num_matrices, n);
k_mix = zeros(num_matrices, n);
k_out = zeros(num_matrices, n);


for m = 1:num_matrices
    % Import the network
    % [A, classes] = import_Network(files(t));
    A = csvread(['data/' networkDir '/Network_' num2str(m) '.txt']);
    n = size(A,1);
    A = sparse((A > 0).*ones(n,n));
    A = A - diag(diag(A));
    classes = csvread(['data/' networkDir '/Classes_' num2str(m) '.txt']);

    % Calculate the parameters
    [c_in(m,:), c_out(m,:), k_in(m,:), k_mix(m,:), k_out(m,:)] = calculate_Parameters_Nodewise(A, classes);
end


c_diff = c_in - c_out;
c_threshold = 2*sqrt((c_in + c_out)/2);
% diff = acc_BTT - acc_NBT;
% below_diff = diff(c_diff < threshold);
% sum(below_diff > 0)
% sum(below_diff < 0)
% mean(below_diff(below_diff > 0))
% mean(below_diff(below_diff < 0))
figure(1); clf; 
hist(reshape(c_in,n*num_matrices,1))
title('C_{in}')
savefig(['Plots/Parameter_Comp/' networkDir '_c_in.fig'])
print(['Plots/Parameter_Comp/' networkDir '_c_in'],'-dpng')
   

figure(2); clf;
hist(reshape(c_out,n*num_matrices,1))
title('C_{out}')
savefig(['Plots/Parameter_Comp/' networkDir '_c_out.fig'])
print(['Plots/Parameter_Comp/' networkDir '_c_out'],'-dpng')

figure(3); clf;
hist(reshape(k_in,n*num_matrices,1))
title('\kappa_{in}')
savefig(['Plots/Parameter_Comp/' networkDir '_k_in.fig'])
print(['Plots/Parameter_Comp/' networkDir '_k_in'],'-dpng')

figure(4); clf;
hist(reshape(k_mix,n*num_matrices,1))
title('\kappa_{mix}')
savefig(['Plots/Parameter_Comp/' networkDir '_k_mix.fig'])
print(['Plots/Parameter_Comp/' networkDir '_k_mix'],'-dpng')

figure(5); clf;
hist(reshape(k_out,n*num_matrices,1))
title('\kappa_{out}')
savefig(['Plots/Parameter_Comp/' networkDir '_k_out.fig'])
print(['Plots/Parameter_Comp/' networkDir '_k_out'],'-dpng')



