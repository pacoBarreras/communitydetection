function [A] = ER_Adj_Communities(n, k, p1, p2)
%ER_Adj Erdos-Renyi Adjacency Matrix Creator
%  Creates an Erdos-Renyi random graph with n nodes using parameter p.
%  The graph is returned as an adjacency matrix A.
%  Parameters:
%   n:  Number of nodes in each community
%   k:  Number of communities
%   p1: Probability of edge creation within a community
%   p2: Probability of edge creation between communities
% 
% A = cell(k,1);
% 
% for comm = 1:k
%     % Create a n-by-n matrix of uniform [0,1] random variables.
%     U = rand(n,n);
% 
%     % On the upper triangular (ABOVE the diagonal), if the uniform random
%     % variable U_{i,j} <= p then we create an edge between i and j.
%     Atemp = triu(U <= p,1);
%     
%     % Since we want an undirected network, copy the upper triangular to the
%     % lower, and then make the resulting matrix sparse (helps dramatically for
%     % later calculations).
%     A{comm} = sparse(Atemp + Atemp');

R = rand(n*k);
in = [ones(n), zeros(n); zeros(n), ones(n)];
out = ones(n*k) - in;
P = p1*in + p2*out;

A = triu(R <= P,1);
A = sparse(A + A');

% ----- OLD (SLOW) METHOD -----
% A = zeros(n*k, n*k);
% 
% for i=1:n*k
%     %On the upper triangular, we randomly create edges
%     for j=(i+1):n*k
%         %We create a random variable X~Unif(0,1), and compare it to p1.
%         %If X < p1 we create an edge.
%         % Here we're in the same community
%         if ceil(i/n) == ceil(j/n) && rand(1) <= p1
%             A(i,j) = 1;
%             % Here we're looking across communities
%         elseif rand(1) <= p2
%             A(i,j) = 1;
%         end
%     end
% end
% 
% % Copy upper triangular entries to the lower triangular
% A = A + A';
% A = sparse(A);

end

