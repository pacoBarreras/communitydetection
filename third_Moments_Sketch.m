% Alpha
a = sym('a');
% Numberator coefficients
% syms('a0','a1','a2','a3');
syms('e1','e2','e3','integer');
assumeAlso(e1 >= 0);
assumeAlso(e1 <= 1);
assumeAlso(e2 >= 0);
assumeAlso(e2 <= 1);
assumeAlso(e3 >= 0);
assumeAlso(e3 <= 1);
% Denominator coefficients
syms('g0','g1','g2','g3');

% Numerator
% Na = a^3*a3 + a^2*a2 + a*a1 + a0;
Na = a^(e1 + e2 + e3);

% Denominator
Da = a^3*g3 + a^2*g2 + a*g1 + g0;

% For finding the root of a single term in the sum
Pa = Da*gradient(Na,a) - Na*gradient(Da,a);

roots = cell(4,1);

% Find the roots of the partial
for i = 0:3
    % Reset assumptions
    syms('e1','e2','e3','integer');
    assumeAlso(e1 >= 0);
    assumeAlso(e1 <= 1);
    assumeAlso(e2 >= 0);
    assumeAlso(e2 <= 1);
    assumeAlso(e3 >= 0);
    assumeAlso(e3 <= 1);
    % Assume they sum to i
    assumeAlso(e1 + e2 + e3 == i);
    
    % Find the numerator and partial again
    Na = a^(e1 + e2 + e3);
    Pa = Da*gradient(Na,a) - Na*gradient(Da,a);
    
    % Find the roots
    roots{i+1} = solve(Pa == 0, a, 'ReturnConditions',true);
    roots{i+1}.a
end