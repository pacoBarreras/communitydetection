% 1. Import network, calculate parameters
% 2. Calculate NBT & BTT
% 3. Classify, measure performance

close all;

% % Get all the files in the random_matrices folder
num_matrices = 100;
networkDir = 'data/SBM_1000_At_Threshold';

% Values of alpha to try
alpha_min = 0.1;
alpha_inc = 0.1;
alpha_max = 3; 
num_alpha = round((alpha_max - alpha_min)/alpha_inc + 1);

c_in = zeros(num_matrices, 1);
c_out = zeros(num_matrices, 1);
k_in = zeros(num_matrices, 1);
k_mix = zeros(num_matrices, 1);
k_out = zeros(num_matrices, 1);
bulk_rad = zeros(num_matrices, 1);
bulk_rad_n = zeros(num_matrices, 1);
acc_BTT = zeros(num_matrices, num_alpha);
acc_BTT = zeros(num_matrices, num_alpha);
acc_BTT = zeros(num_matrices, num_alpha);
acc_BTT_n = zeros(num_matrices, num_alpha);
lam_1 = zeros(num_matrices, num_alpha);
lam_2 = zeros(num_matrices, num_alpha);
lam_1_n = zeros(num_matrices, num_alpha);
lam_2_n = zeros(num_matrices, num_alpha);

% For eigenvalue validation
conv_NBT = ones(num_matrices,1);
conv_BTT = ones(num_matrices,num_alpha);

% For allowing more iterations of eigenvalue calculation, to ensure
% convergence
opts.maxit = 5000;

for t = 1:num_matrices
    % Import the network
    % [A, classes] = import_Network(files(t));
    A = csvread([networkDir '/Network_' num2str(t) '.txt']);
    n = size(A,1);
    A = sparse((A > 0).*ones(n,n));
    A = A - diag(diag(A));
    classes = csvread([networkDir '/Classes_' num2str(t) '.txt']);
    
    % Make a graph object from A
    G = digraph(A);
    % Get the edges. Column 1 is source node, 2 is target node, 3 is weight.
    E = table2array(G.Edges);
    m = size(E,1);
    
    % Calculate the parameters
    [c_in(t), c_out(t), k_in(t), k_mix(t), k_out(t)] = calculate_Parameters(A, classes);
    bulk_rad(t) = sqrt( (c_in(t) + c_out(t))/2 );
    
    % For normalized BTT
    d = sum(A,2);
    frac = d./(d-1);
    frac(isinf(frac)) = 0;
    bulk_rad_n(t) = sqrt(mean(frac)/mean(d));
    
    %%------------------------%%
    %           BTT            %
    %%------------------------%%
    % Try different values of alpha
    parfor a = 1:num_alpha
        alpha = (a - 1)*alpha_inc + alpha_min;

        % Find BTT, unnormalized
        BTT = makeBTT(A, 'unif', alpha, 0);

        % Measure the spectral gap, as a function of alpha
        [~, val] = eigs(BTT, 50, 'LM', opts);
        val = diag(val);
        [~, eigInd] = min(abs(imag(val(2:end))));
        lam_1(t,a) = val(1);
        lam_2(t,a) = val(eigInd + 1);

        % Classify & measure the accuracy
        [acc_BTT_LM(t,a), ~] = classify(A, BTT, classes, 'largestabs');
        [acc_BTT_LR(t,a), ~] = classify(A, BTT, classes, 'largestpurereal');
        [acc_BTT_LRP(t,a), ~] = classify(A, BTT, classes, 'largestreal');
        
        % Find BTT, normalized
        BTT = makeBTT(A, 'unif', alpha, 1);

        % Measure the spectral gap, as a function of alpha
        [~, val] = eigs(BTT, 50, 'LM', opts);
        val = diag(val);
        [~, eigInd] = min(abs(imag(val(2:end))));
        lam_1_n(t,a) = val(1);
        lam_2_n(t,a) = val(eigInd + 1);

        % Classify & measure the accuracy
        [acc_BTT_n_LM(t,a), ~] = classify(A, BTT, classes, 'largestabs');
        [acc_BTT_n_LR(t,a), ~] = classify(A, BTT, classes, 'largestpurereal');
        [acc_BTT_n_LRP(t,a), ~] = classify(A, BTT, classes, 'largestreal');
    end
    % plot(alpha_min:alpha_inc:alpha_max, squeeze(spectral_gap(1,t,:)))
    disp(t)
end

c_diff = c_in - c_out;
c_threshold = 2*sqrt((c_in + c_out)/2);

% Optimal accuracy values
[mB, mBi] = max(acc_BTT, [], 2);
[mB_n, mBi_n] = max(acc_BTT_n, [], 2);

% Crossing values
[~, cBi] = max(lam_2 > bulk_rad*ones(1,30),[],2);
[~, cBi_n] = max(lam_2_n > bulk_rad_n*ones(1,30),[],2);
cB = diag(acc_BTT(:,cBi));
cB_n = diag(acc_BTT_n(:,cBi_n));

alphas = alpha_min:alpha_inc:alpha_max;

mNBT = acc_BTT(:,alphas == 1);
mNBT_n = acc_BTT_n(:,alphas == 1);

pdOpt = (mB - mNBT)./mNBT*100;
pdOpt_n = (mB_n - mNBT_n)./mNBT_n*100;
pdC = (cB - mNBT)./mNBT*100;
pdC_n = (cB_n - mNBT_n)./mNBT_n*100;

% Plot for both BTT
figure(1); clf; hold on;
plot(pdOpt)
plot(pdC)
plot(pdOpt_n)
plot(pdC_n)
hold off;
title('Comparison between Unnormalized and Flow BTT(\alpha)')
xlabel('Network')
ylabel('Percent Diff. from NBT (%)')
legend('Opt. \alpha','Cross \alpha','Opt. \alpha (Flow)','Cross \alpha (Flow)','location','best')

% Plot for the unnormalized BTT
figure(2); clf; hold on;
plot(pdOpt)
plot(pdC)
hold off;
title('Unnormalized BTT(\alpha)')
xlabel('Network')
ylabel('Percent Diff. from NBT (%)')
legend('Opt. \alpha','Cross \alpha','location','best')

% Plot for the normalized BTT
figure(3); clf; hold on;
plot(pdOpt_n)
plot(pdC_n)
hold off;
title('Flow BTT(\alpha)')
xlabel('Network')
ylabel('Percent Diff. from NBT (%)')
legend('Opt. \alpha (Flow)','Cross \alpha (Flow)','location','best')

% % Plot accuracy with 95% confidence intervals
% figure(1);
% clf;
% hold on
%     %BTT
%     for type = 1:num_Types
%         mean_BTT = squeeze(mean(acc_BTT(type,:,:),2));
%         plot(alpha_min:alpha_inc:alpha_max,mean_BTT,'Color',colours(type,:))
%     end
%     % Lap
%     plot(alpha_min:alpha_inc:alpha_max,mean_Lap*ones(num_alpha,1),'-g')
%     %NBT
%     plot(alpha_min:alpha_inc:alpha_max,mean_NBT*ones(num_alpha,1),'-k')
% hold off
% xlim([alpha_min alpha_max])
% xlabel('\alpha')
% ylabel('Accuracy')
% types{num_Types+1} = 'Laplacian'; types{num_Types+2} = 'NBT';
% legend(types,'Location','best')
% title(['Accuracy with varying \alpha, ' num2str(num_matrices) ' networks, c_{in} = ' num2str(mean(c_in)) ', c_{out} = ' num2str(mean(c_out))])