function [c_in, c_out, k_in, k_mix, k_out] = calculate_Parameters(A, classes)
    % Number of nodes in network
    n = size(A,1);
    
    % Whether a node is in a given class
    class_1 = double(classes == 1);
    class_2 = double(classes == 2);
    
    % Matrix of 1's where the edge goes INSIDE the community
    in = class_1*class_1' + class_2*class_2';
    
    % Matrix of 1's where the edge goes OUTSIDE the community
    out = ones(n,n) - in;
    
    % Average degree inside and across communities
    c_in = full(mean(sum(A.*in,2)))*2;
    c_out = full(mean(sum(A.*out,2)))*2;

    % Make a graph object from A
    G = digraph(A);
    % Get the edges. Column 1 is source node, 2 is target node, 3 is weight.
    E = table2array(G.Edges);
    m = size(E,1);
    
    % To count the number of wedges and triangles
    tri_in = 0;
    wedge_in = 0;
    tri_mix = 0;
    wedge_mix = 0;
    tri_out = 0;
    wedge_out = 0;
    
    % Calculate clustering coefficients
    for i = 1:m
        for j = 1:m
            if E(i,2) == E(j,1) % We have a wedge: i ends where j begins
                % Check class membership of nodes in wedge
                if (classes(E(i,1)) == classes(E(j,2))) && (classes(E(i,1)) == classes(E(i,2))) % All same class
                    wedge_in = wedge_in + 1;
                elseif classes(E(i,1)) == classes(E(j,2)) % Non-wedge nodes in same class (also, wedge node different)
                    wedge_out = wedge_out + 1;
                else % Non-wedge nodes in different class
                    wedge_mix = wedge_mix + 1;
                end
                
                if A(E(j,2), E(i,1)) == 1 %ismember([E(j,2), E(i,1),  1], E, 'rows') % It's a triangle: there's an edge from where j ends to where i begins
                    % Check class membership of nodes in triangle
                    if classes(E(i,1)) == classes(E(j,2)) && classes(E(i,1)) == classes(E(i,2)) % All same class
                        tri_in = tri_in + 1;
                    elseif classes(E(i,1)) == classes(E(j,2)) % Non-wedge nodes in same class (also, wedge node different)
                        tri_out = tri_out + 1;
                    else % Non-wedge nodes in different class
                        tri_mix = tri_mix + 1;
                    end
                end
            end
        end
    end
    
    k_in = tri_in/wedge_in;
    k_mix = tri_mix/wedge_mix;
    k_out = tri_out/wedge_out;
end