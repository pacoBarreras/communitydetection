num_alpha = 10000;
v = zeros(n,num_alpha);

for alpha = 1:num_alpha
    nbt3 = f(alpha)^3;
    
    for i = 1:m
        v(E(i,2),alpha) = v(E(i,2),alpha) + nbt3(i,i);
    end
end

figure(1);
clf;
plot(mean(v,1))
title('Average probability of performing a closed length-3 walk')
xlabel('\alpha')
ylabel('1/n \Sigma P(perform closed length-3 walk)')

figure(2);
clf;
hold on
for i = 1:n
    plot(v(i,:))
end
hold off
title('Individual probability of performing a closed length-3 walk')
xlabel('\alpha')
ylabel('P(perform closed length-3 walk for node i)')