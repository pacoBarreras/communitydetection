function [acc,class_est] = classify(A,M,classes,type)
% Given a NBT/BTT matrix M, classifies the network from A into two
% communities.
% --- INPUTS:
%   A           - adjacency matrix of (possibly directed) network
%   M           - NBT or BTT matrix
%   classes     - true classes of nodes. If provided will be used to
%                 compute accuracy.
%
% --- OUTPUT:
%   acc         - classification accuracy
%   class_est   - class estimates
    
    % Make a graph object from A
    G = digraph(A);
    n = size(A,1);
    % Get the edges. Column 1 is source node, 2 is target node, 3 is weight.
    E = table2array(G.Edges);
    m = size(E,1);

    % For allowing more iterations of eigenvalue calculation, to ensure
    % convergence
    opts.maxit = 5000;
    
    % Find second-largest eigenvalues' eigenvectors, based on type
    switch type
        case 'largestpurereal'
            [vec, val] = eigs(M, m, 'largestreal', opts);
            val = diag(val);
            [~, eigInd] = min(abs(imag(val(2:end))));
        case 'largestreal'
            [vec, ~] = eigs(M, m, 'largestreal', opts);
            eigInd = 1;
        case 'largestabs'
            [vec, ~] = eigs(M, m, 'largestabs', opts);
            eigInd = 1;
    end
    
    % Classify nodes based on the sign of the sum of the components of the
    % eigenvector for edges pointing into the node.
    class_est = zeros(n,1);
    
    % Sum up the components, as described above
    for i = 1:m
        % Add this component's value to the target node of this edge
        class_est(E(i,2)) = class_est(E(i,2)) + real( vec(i,eigInd + 1) );
    end
    
    % Classify the nodes based on the sign of the sum
    class_est = (class_est > 0) + 1;
    
    % If true classes were provided, compute accuracy
    if ~isempty(classes) > 0
        % Measure the accuracy
        acc = mean(class_est == classes);

        % Correct for communities flipping
        if acc < 0.5
            acc = 1 - acc;
        end
    end
end