num_matrices = 1000;
degree = 5;
p = zeros(num_matrices,degree+1);

col_mat = hsv(num_matrices);

alphas = (alpha_min:alpha_inc:alpha_max)';

% Fit polynomials to the accuracy vs alpha curves and plot them
figure(1); clf; hold on;
for m = 1:num_matrices
    p(m,:) = polyfit(alphas, squeeze(acc_BTT(1,m,:)), degree);
    plot(alphas, squeeze(acc_BTT(1,m,:)), 'Color', col_mat(m,:));
    plot(alphas, polyval(p(m,:), alphas), 'Color', col_mat(m,:), 'Linestyle', '--')
end
hold off;

% Plot transitivity vs optimal alpha
[mv, mi] = max(squeeze(acc_BTT(1,:,:)),[],2);
opt_alphas = alphas(mi);
spectral_gaps = diag(squeeze(spectral_gap(1,:,mi)));

figure(2); clf; hold on;
scatter(k_in, opt_alphas, '.r')
scatter(k_mix, opt_alphas, '.b')
scatter(k_out, opt_alphas, '.g')
hold off;

% Perform a linear regression
sg_opt = diag(squeeze(spectral_gap(1,:,mi))); % Spectral gap at optimal alpha
X = [ones(size(k_in)) c_in c_out k_in k_mix k_out];
a = X\opt_alphas; % Find coefficients
Y = X*a;
Yinds = round((Y - alpha_min)/alpha_inc + 1); % Find corresponding indices for alpha
Yinds(Yinds <= 0) = 1;
Yacc = diag(squeeze(acc_BTT(1,:,Yinds))); % find accuracy for alpha values found via regression

figure(3); clf; hold on;
plot(Yacc); % Plot accuracy of regression alphas
plot(mv); % Plot accuracy of optimal alphas
plot(acc_NBT); % Plot accuracy of NBT
title('Accuracy Comparison')
xlabel('Network')
ylabel('Accuracy (%)')
legend('Regression','Optimal \alpha', 'NBT', 'Location', 'best')

% Find percent difference (percentage increase) from NBT
pd = (mv - acc_NBT)./acc_NBT*100;
pdY = (Yacc - acc_NBT)./acc_NBT*100;
mean(pd)
mean(pdY)





