function [NBT] = makeBTT(A, type, alpha, normalize)
% Given an adjacency matrix A, returns the non-backtracking matrix NBT.
% --- INPUTS:
%   A           - adjacency matrix of (possibly directed) network
%   type        - kind of BTT matrix to make
%
% --- OUTPUT:
%   BTT         - non-backtracking matrix, with triangles weighted by alpha

% Make a graph object from A
G = digraph(A);
% Get the edges. Column 1 is source node, 2 is target node, 3 is weight.
E = table2array(G.Edges);

m = size(E,1);
n = size(A,1);

NBT = zeros(m,m);
val = zeros(m);

switch type
    % No different weights
    case 'NBT'
        val = ones(m);
    % Uniform weights
    case 'unif'
        val = alpha*ones(m);
    % Weight based on degree of node we're at
    case 'deg-at'
        mat = alpha*sum(A,2);
        for i = 1:m
           val(i,:) = mat(E(i,2))*ones(1,m); 
        end
    % Weight based on degree of node we came from
    case 'deg-from'
        mat = alpha*sum(A,2);
        for i = 1:m
           val(i,:) = mat(E(i,1))*ones(1,m); 
        end
    % Weight based on degree of node we're going to
    case 'deg-to'
        mat = alpha*sum(A,2);
        for j = 1:m
           val(:,j) = mat(E(j,2))*ones(m,1); 
        end
    % Weight based on inverse of degree of node we came from
    case 'deg-inv'
        mat = alpha./sum(A,2);
        for i = 1:m
           val(i,:) = mat(E(i,1))*ones(1,m); 
        end
    % Weight based on number of triangles from node to itself
    case 'tri'
        mat = alpha*diag(A^3);
        for i = 1:m
           val(i,:) = mat(E(i,2))*ones(1,m); 
        end
    % Weight based oninverse of number of triangles from node to itself
    case 'tri-inv'
        mat = alpha./diag(A^3);
        for i = 1:m
           val(i,:) = mat(E(i,1))*ones(1,m); 
        end
    % Weight based on local transitivity of where we are
    case 'trans'
        % Calculate local clustering
        C = local_clustering(A);
        mC = mean(C);
        for i = 1:m
            for j = 1:m
                val(i,j) = alpha./(C(E(i,2))/mC); 
            end
        end
    % Weight based on number of neighbours in common
    case 'nic'
        for i = 1:m
        	val(i,:) = alpha*(A(E(i,1),:)*A(E(i,2),:)');
        end
    % Weight based on inverse number of neighbours in common
    case 'nic-inv'
        for i = 1:m
        	val(i,:) = alpha./(A(E(i,1),:)*A(E(i,2),:)' + 1);
        end
end

for i = 1:m
    for j = 1:m
        % Check if tail of ith edge matches head of jth edge, AND we're not backtracking
        if E(i,2) == E(j,1) && E(j,2) ~= E(i,1)
            if A(E(j,2),E(i,1)) > 0 % Check if there's an edge from tail of j to head of i
            	NBT(i,j) = val(i,j);
            else
                NBT(i,j) = 1;
            end
        end
    end
end

if normalize
    rowSum = sum(NBT,2);
    % Ensure that if there are any empty rows, we don't divide by 0.
    rowSum(rowSum == 0) = -1;

    % Normalize the NBT
    NBT = NBT./rowSum;
end

NBT = sparse(NBT);
% x = 0:0.01:2*pi;
% 
% figure;
% clf
% hold on
%     plot(cos(x), sin(x), '-b');                   % Plot the unit circle
%     plot([0 1], [0 0], '-b');                     % Plot line at 0 rad
%     plot([0 cos(2*pi/3)], [0 sin(2*pi/3)], '-b')  % Plot line at 2pi/3 rad
%     plot([0 cos(4*pi/3)], [0 sin(4*pi/3)], '-b')  % Plot line at 4pi/3 rad
%     scatter(real(ev), imag(ev),'.r');             % Plot the eigenvalues
% hold off
% title('Eigenvalues of NBT Matrix')
% xlabel('Real Component')
% ylabel('Imaginary Component')