% 1. Import network, calculate parameters
% 2. Calculate NBT & BTT
% 3. Classify, measure performance

close all;

% % Get all the files in the random_matrices folder
% data = dir('random_matrices');
network = 1;
networkDir = 'data/SA_Networks_Rewire_Below_Threshold';
% files = strings(num_matrices,1);
% 
% for f = 3:(num_matrices + 3)
%     files(f - 2) = strcat('random_matrices/',data(f).name);
% end

normalized = 1;

% Values of alpha to try
alpha_min = 0.1;
alpha_inc = 0.01;
alpha_max = 3;
num_alpha = round((alpha_max - alpha_min)/alpha_inc + 1);

% Accuracy
acc_BTT_LM = zeros(num_alpha,1);
acc_BTT_LR = zeros(num_alpha,1);
acc_BTT_LP = zeros(num_alpha,1);

% For eigenvalues
spectra = cell(num_alpha,1);

% For allowing more iterations of eigenvalue calculation, to ensure
% convergence
opts.maxit = 5000;

% Import the network
% [A, classes] = import_Network(files(t));
A = csvread([networkDir '/Network_' num2str(network) '.txt']);
n = size(A,1);
A = sparse((A > 0).*ones(n,n));
A = A - diag(diag(A));
classes = csvread([networkDir '/Classes_' num2str(network) '.txt']);

% Make a graph object from A
G = digraph(A);
% Get the edges. Column 1 is source node, 2 is target node, 3 is weight.
E = table2array(G.Edges);
m = size(E,1);

% Calculate the parameters
[c_in, c_out, k_in, k_mix, k_out] = calculate_Parameters(A, classes);

%%------------------------%%
%           BTT            %
%%------------------------%%
% Try different values of alpha
parfor a = 1:num_alpha
    alpha = (a - 1)*alpha_inc + alpha_min;

    % Find BTT weighing by neighbours in common
    BTT = makeBTT(A, 'unif', alpha, normalized);

    % Classify & measure the accuracy
    [acc_BTT_LM(a), ~] = classify(A, BTT, classes, 'largestabs');
    [acc_BTT_LR(a), ~] = classify(A, BTT, classes, 'largestpurereal');
    [acc_BTT_LP(a), ~] = classify(A, BTT, classes, 'largestreal');

    % Calculate spectrum
    spectra{a} = eigs(BTT,m);
    disp(a)
end

c_diff = c_in - c_out;
c_threshold = 2*sqrt((c_in + c_out)/2);
% diff = acc_BTT - acc_NBT;
% below_diff = diff(c_diff < threshold);
% sum(below_diff > 0)
% sum(below_diff < 0)
% mean(below_diff(below_diff > 0))
% mean(below_diff(below_diff < 0))

[mB, mBi] = max(acc_BTT, [], 1);

% For plotting a circle
theta = 0:0.01:2*pi;
xC = cos(theta);
yC = sin(theta);

xmin = min(real(spectra{end}));
xmax = max(real(spectra{end}));
ymin = min(imag(spectra{end}));
ymax = max(imag(spectra{end}));

figure(1);
for a = 1:num_alpha
    clf; hold on;
    % Plot eigenvalues, and a circle representing the bulk radius
    scatter(real(spectra{a}),imag(spectra{a}),'.b')
    r = sqrt(spectra{a}(1));
    plot(r*xC, r*yC, '--r')
    hold off;
    title(['\alpha = ' num2str((a - 1)*alpha_inc + alpha_min) ', Accuracy = ' num2str(acc_BTT(a))])
    xlim([xmin xmax])
    ylim([ymin ymax])
    pause(0.01);
end

figure(3)
plot(acc_BTT)