% 1. Import network, calculate parameters
% 2. Calculate NBT & BTT
% 3. Classify, measure performance

close all;

% % Get all the files in the random_matrices folder
num_matrices = 100;
networkDir = 'data/SA_Networks_Rewire';

% Values of alpha to try
alpha_min = 1;
alpha_inc = 1;
alpha_max = 1; 
num_alpha = (alpha_max - alpha_min)/alpha_inc + 1;

% Node-specific types to try
types = {'unif'; 'nic'; 'nic-inv'; 'tri'; 'tri-inv'; 'deg-from'; 'deg-at'; 'deg-to'; 'deg-inv'; 'trans'};
num_Types = size(types,1);

c_in = zeros(num_matrices, 1);
c_out = zeros(num_matrices, 1);
k_in = zeros(num_matrices, 1);
k_mix = zeros(num_matrices, 1);
k_out = zeros(num_matrices, 1);
acc_NBT = zeros(num_matrices, 1);
acc_Lap = zeros(num_matrices, 1);
acc_BTT = zeros(num_Types, num_matrices, num_alpha);
third_moment_NBT = zeros(num_matrices, 1);
third_moment_BTT = zeros(num_matrices, num_alpha);

% For eigenvalue validation
conv_NBT = ones(num_matrices,1);
conv_BTT = ones(num_matrices,num_alpha);

% For allowing more iterations of eigenvalue calculation, to ensure
% convergence
opts.maxit = 5000;

for t = 1:num_matrices
    % Import the network
    % [A, classes] = import_Network(files(t));
    A = csvread([networkDir '/Network_' num2str(t) '.txt']);
    n = size(A,1);
    A = sparse((A > 0).*ones(n,n));
    A = A - diag(diag(A));
    classes = csvread([networkDir '/Classes_' num2str(t) '.txt']);
    
    % Make a graph object from A
    G = digraph(A);
    % Get the edges. Column 1 is source node, 2 is target node, 3 is weight.
    E = table2array(G.Edges);
    m = size(E,1);
    
    % Calculate the parameters
    [c_in(t), c_out(t), k_in(t), k_mix(t), k_out(t)] = calculate_Parameters(A, classes);
    
    %%------------------------%%
    %           NBT            %
    %%------------------------%% 
    % Find NBT (final digit 0=unnormalized, 1=normalized, i.e., flow matrix)
    NBT = makeNBT(A, 1, 1);
    
    % Classify & measure the accuracy
    [acc_NBT(t), ~] = classify(A, NBT, classes);
    
    % Calculate third moment
    % third_moment_NBT(t) = 1/m*trace(NBT^3); %mean(NBTval.^3);
    
    %%------------------------%%
    %        Laplacian         %
    %%------------------------%%
    L = diag(sum(A,1)) - A;
    [Lapvec, Lapval] = eigs(L, 2, 'LM');
    
    class_Lap = (Lapvec(:,2) > 0) + 1;
    acc_Lap(t) = mean(classes == class_Lap);
    
    if acc_Lap(t) < 0.5
            acc_Lap(t) = 1 - acc_Lap(t);
    end
    
    %%------------------------%%
    %           BTT            %
    %%------------------------%%
    % Try different values of alpha
    parfor a = 1:num_alpha
        for type = 1:num_Types
            alpha = (a - 1)*alpha_inc + alpha_min;

            % Find BTT weighing according to type
            BTT = makeBTT(A, types{type}, alpha, 1);

            % Classify & measure the accuracy
            [acc_BTT(type,t,a), ~] = classify(A, BTT, classes);
        end
    end
    
    disp(t)
end

c_diff = c_in - c_out;
c_threshold = 2*sqrt((c_in + c_out)/2);

mean_NBT = mean(acc_NBT);

mean_Lap = mean(acc_Lap);

[mB, mBi] = max(acc_BTT, [], 2);

colours = hsv(num_Types);

% Plot accuracy with 95% confidence intervals
figure(1);
clf;
hold on
    %BTT
    for type = 1:num_Types
        mean_BTT = squeeze(mean(acc_BTT(type,:,:),2));
        plot(alpha_min:alpha_inc:alpha_max,mean_BTT,'Color',colours(type,:))
    end
    % Lap
    plot(alpha_min:alpha_inc:alpha_max,mean_Lap*ones(num_alpha,1),'-g')
    %NBT
    plot(alpha_min:alpha_inc:alpha_max,mean_NBT*ones(num_alpha,1),'-k')
hold off
xlim([alpha_min alpha_max])
xlabel('\alpha')
ylabel('Accuracy')
types{num_Types+1} = 'Laplacian'; types{num_Types+2} = 'NBT';
legend(types,'Location','best')
title(['Accuracy with varying \alpha, ' num2str(num_matrices) ' networks, c_{in} = ' num2str(mean(c_in)) ', c_{out} = ' num2str(mean(c_out))])

% % Plot accuracy showing 1 standard deviation
% std_NBT = std(acc_NBT);
% figure(2);
% clf;
% hold on
%     % Lap
%     plot(alpha_min:alpha_inc:alpha_max,mean_Lap*ones(num_alpha,1),'-g')
%     % NBT
%     plot(alpha_min:alpha_inc:alpha_max,mean_NBT*ones(num_alpha,1),'-r')
%     plot(alpha_min:alpha_inc:alpha_max,(mean_NBT + std_NBT)*ones(num_alpha,1),'--r')
%     plot(alpha_min:alpha_inc:alpha_max,(mean_NBT - std_NBT)*ones(num_alpha,1),'--r')
%     % BTT
%     for type = 1:num_Types
%         mean_BTT = squeeze(mean(acc_BTT(type,:,:),2));
%         std_BTT = squeeze(std(acc_BTT(type,:,:)));
%         plot(alpha_min:alpha_inc:alpha_max,mean_BTT,'-b')
%         plot(alpha_min:alpha_inc:alpha_max,mean_BTT + std_BTT,'--b')
%         plot(alpha_min:alpha_inc:alpha_max,mean_BTT - std_BTT,'--b')
%     end
% hold off
% xlim([alpha_min alpha_max])
% xlabel('\alpha')
% ylabel('Accuracy')
% legend('NBT','Laplacian','BTT(\alpha)','Location','best')
% title(['Accuracy & Std. Dev. with varying \alpha, ' num2str(num_matrices) ' networks'])

% % Plot the network
% colors = [(classes == 1), zeros(n,1),(classes == 2)];
% markers = char((classes == 1)*'o' + (classes == 2)*'x');
% 
% figure(2);
% p = plot(graph(A),'layout','force','Nodelabel',[],'Nodecolor',colors);
% %p.highlight(find(class_BTT ~= classes), 'Marker','x','Markersize',10);

% % For plotting eigenvalues
% alpha = (mBi(end) - 1)*alpha_inc + alpha_min;
% BTT = makeNBT(A, alpha, 1);
% [BTTvec, BTTval] = eigs(BTT, 50, 'LM', opts);
% BTTval = diag(BTTval);
% 
% figure(3);
% clf;
% hold on
%     scatter(real(NBTval),imag(NBTval),'.r')
%     scatter(real(BTTval),imag(BTTval),'.b')
% hold off
% legend('NBT','BTT','Location','best')
% title(['Example of eigenvalues, \alpha = ' num2str(alpha)])