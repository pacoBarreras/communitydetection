# -*- coding: utf-8 -*-
"""
Created on Mon Mar 19 15:31:01 2018

@author: Mikhail
"""
import random as rnd
import numpy as np
import networkx as nx
import scipy as sp

num_networks = 10;
n = 20;
community_threshold = n/2; # node index at which we switch from community 1 to 2
c_in = 12;       # Twice the average degree WITHIN communities
c_out = 2;      # Twice the average degree ACROSS communities
k_in = 0.2     # Clustering coefficient WITHIN communities
k_out = 0.01;    # Clustering coeff, both corners ACROSS communities
k_het = 0.01;   # Clustering coeff, one corner in each community
classes = np.zeros([n,1]);

def pickTriangle(i, mat):
    # Find candidate vertices
    v = list(np.nonzero(mat)[0])
    # For prioritizing candidates that want >1 connections (so we don't end up
    # with a single node that wants 2 connections at the end).
    vBig = list(np.nonzero(np.array(mat) > 1)[0])

    # Remove node i, if it's there
    if i in v:
        v.remove(i);
    if i in vBig:
        vBig.remove(i);
    
    # There are not enough other candidates
    if len(v) <= 1:
        return([-1, -1]);
    
    # Select first vertex, prioritizing nodes wanting >1 extra triangles
    if len(vBig) > 0:
        v1 = rnd.choice(vBig);
        vBig.remove(v1);
    else:
        v1 = rnd.choice(v);
    
    # Remove node v1
    v.remove(v1);
        
    # Select second vertex, prioritizing as before
    if len(vBig) > 0:
        v2 = rnd.choice(vBig);
    else:
        v2 = rnd.choice(v);
        
    return( [v1, v2] );
        
def pickNode(i, mat):
    # Find candidate vertices
    v = list(np.nonzero(mat)[0])
    # For prioritizing candidates that want >1 connections (so we don't end up
    # with a single node that wants 2 connections at the end).
    vBig = list(np.nonzero(np.array(mat) > 1)[0])
    
    # Remove node i, if it's there
    if i in v:
        v.remove(i);
    if i in vBig:
        vBig.remove(i);
        
    # There are no other candidates
    if len(v) == 0:
        return(-1);
    
    # If there are nodes that still want 2 or more connections, prioritize them
    if len(vBig) > 0:
        v1 = rnd.choice(vBig);
    else:
        v1 = rnd.choice(v);
        
    return(v1);

# Receives degree and clustering coefficients, and returns a degree distribution
# for use in generating a Newman network.
def degreeDistribution(n, c_in, c_out, k_in, k_out, k_het, doPoisson=True):
    t_in_mean = k_in*sp.special.comb(c_in/2, 2);
    t_out_mean = k_out*sp.special.comb(c_out/2, 2);
    t_het_mean = k_het*(c_in*c_out/4);
    s_in_mean = c_in/2 - t_het_mean - 2*t_in_mean;
    s_out_mean = c_out/2 - t_het_mean - 2*t_out_mean;
    
    # We want the parameters to be generated randomly
    if doPoisson:
        t_in = np.random.poisson(t_in_mean, n);
        t_out = np.random.poisson(t_out_mean, n);
        t_het = np.random.poisson(t_het_mean, n);
        s_in = np.random.poisson(s_in_mean, n);
        s_out = np.random.poisson(s_out_mean, n);
    else:
        t_in = [t_in_mean for i in range(n)];
        t_out = [t_out_mean for i in range(n)];
        t_het = [t_het_mean for i in range(n)];
        s_in = [s_in_mean for i in range(n)];
        s_out = [s_out_mean for i in range(n)];
        
    return( [t_in, t_out, t_het, s_in, s_out] );


############################################
#
#       OLD METHOD (SEQUENTIAL)
#
############################################

#def makeNewman(n, community_threshold, c_in, c_out, k_hom, k_in, k_out, show_plot=True):
#    # Initialize the network
#    A = np.zeros([n,n]);
#    
#    # t_i = # of triangles node i is in; total must be multiple of 3
#    # s_i = # of single (non-triangle) edges for node i; total must be multiple of 2
#    [t_hom, t_in, t_out, s_in, s_out] = degreeDistribution(n, c_in, c_out, k_hom, k_in, k_out);
#    
#    # Loop through all nodes, creating as many triangles and edges as necessary.
#    for i in range(n):
#        # We still want to make triangles WITHIN communities
#        while t_in[i] > 0:
#            # Find candidate vertices (i.e., i s.t. t_in[i] > 0)
#            if i < community_threshold:    # We're in community 1
#                [v1, v2] = pickTriangle(i, t_in[0:community_threshold]);
#            else:                           # We're in community 2
#                [v1, v2] = pickTriangle(i - community_threshold, t_in[community_threshold:n]);
#                v1 = v1 + community_threshold;
#                v2 = v2 + community_threshold;
#        
#            # Update the adjacency matrix
#            A[i,v1] = A[i,v1] + 1;      # i -> v1
#            A[v1,i] = A[v1,i] + 1;      # v1 -> i
#            A[i,v2] = A[i,v2] + 1;      # i -> v2
#            A[v2,i] = A[v2,i] + 1;      # v2 -> i
#            A[v1,v2] = A[v1,v2] + 1;    # v1 -> v2
#            A[v2,v1] = A[v2,v1] + 1;    # v2 -> v1
#            
#            # Update triangle counts
#            t_in[i] = t_in[i] - 1;
#            t_in[v1] = t_in[v1] - 1;
#            t_in[v2] = t_in[v2] - 1;
#        
#        # We still want to make edges WITHIN communities
#        while s_in[i] > 0:
#            # Pick a node to make an edge with
#            if i < community_threshold:    # We're in community 1
#                v1 = pickNode(i, s_in[0:community_threshold]);
#            else:                           # We're in community 2
#                v1 = pickNode(i - community_threshold, s_in[community_threshold:n]);
#                v1 = v1 + community_threshold;
#    
#            # Update the adjacency matrix
#            A[i,v1] = A[i,v1] + 1;      # i -> v1
#            A[v1,i] = A[v1,i] + 1;      # v1 -> i
#            
#            # Update edge counts
#            s_in[i] = s_in[i] - 1;
#            s_in[v1] = s_in[v1] - 1;
#            
#        # We still want to make triangles ACROSS communities
#        while t_out[i] > 0:
#            # Find candidate vertices (i.e., i s.t. t_in[i] > 0)
#            if i >= community_threshold:    # We're in community 1
#                [v1, v2] = pickTriangle(i, t_out[0:community_threshold]);
#            else:                           # We're in community 2
#                [v1, v2] = pickTriangle(i, t_out[community_threshold:n]);
#                v1 = v1 + community_threshold;
#                v2 = v2 + community_threshold;
#        
#            # Update the adjacency matrix
#            A[i,v1] = A[i,v1] + 1;      # i -> v1
#            A[v1,i] = A[v1,i] + 1;      # v1 -> i
#            A[i,v2] = A[i,v2] + 1;      # i -> v2
#            A[v2,i] = A[v2,i] + 1;      # v2 -> i
#            A[v1,v2] = A[v1,v2] + 1;    # v1 -> v2
#            A[v2,v1] = A[v2,v1] + 1;    # v2 -> v1
#            
#            # Update triangle counts
#            t_out[i] = t_out[i] - 1;
#            t_out[v1] = t_out[v1] - 1;
#            t_out[v2] = t_out[v2] - 1;
#        
#        # We still want to make edges ACROSS communities
#        while s_out[i] > 0:   
#            # Pick a node to make an edge with
#            if i >= community_threshold:    # We're in community 1
#                v1 = pickNode(i, s_out[0:community_threshold]);
#            else:                           # We're in community 2
#                v1 = pickNode(i + community_threshold, s_out[community_threshold:n]);
#                v1 = v1 + community_threshold;
#    
#            # Update the adjacency matrix
#            A[i,v1] = A[i,v1] + 1;      # i -> v1
#            A[v1,i] = A[v1,i] + 1;      # v1 -> i
#            
#            # Update edge counts
#            s_out[i] = s_out[i] - 1;
#            s_out[v1] = s_out[v1] - 1;
#            
#    # Make and plot a graph object from the adjacency matrix A
#    if show_plot:
#        G = nx.from_numpy_matrix(A);
#        pos = nx.spring_layout(G);
#        nx.draw_networkx_nodes(G,pos,nodelist=range(community_threshold),node_color='r');
#        nx.draw_networkx_nodes(G,pos,nodelist=range(community_threshold,n),node_color='b');
#        nx.draw_networkx_edges(G,pos)
#    
#    return A


def makeNewman(n, community_threshold, c_in, c_out, k_in, k_out, k_het, doPoisson=True, show_plot=True):
    # Initialize the network
    A = np.zeros([n,n]);
    
    # t_i = # of triangles node i is in; total must be multiple of 3
    # s_i = # of single (non-triangle) edges for node i; total must be multiple of 2
    
    t_in = [3 for i in range(n)];
    t_out = [0 for i in range(n)];
    t_het = [0 for i in range(n)];
    s_in = [3 for i in range(n)];
    s_out = [1 for i in range(n)];
    #[t_in, t_out, t_het, s_in, s_out] = degreeDistribution(n, c_in, c_out, k_in, k_out, k_het, doPoisson);
    
    #print 'Degree distribution made.'
    
    # Loop through all nodes, creating as many triangles and edges as necessary.
    while sum(t_in[0:community_threshold]) >= 2 or sum(t_in[community_threshold:n]) >= 2:   # We can still make some triangles
        for i in range(n):
            # We still want to make triangles WITHIN communities
            if t_in[i] > 0:
                # Find candidate vertices (i.e., i s.t. t_in[i] > 0)
                if i < community_threshold:    # We're in community 1
                    [v1, v2] = pickTriangle(i, t_in[0:community_threshold]);
                else:                           # We're in community 2
                    [v1, v2] = pickTriangle(i - community_threshold, t_in[community_threshold:n]);
                    v1 = v1 + community_threshold;
                    v2 = v2 + community_threshold;
                
                if v1 != -1: # We were able to find a match
                    # Update the adjacency matrix
                    A[i,v1] = A[i,v1] + 1;      # i -> v1
                    A[v1,i] = A[v1,i] + 1;      # v1 -> i
                    A[i,v2] = A[i,v2] + 1;      # i -> v2
                    A[v2,i] = A[v2,i] + 1;      # v2 -> i
                    A[v1,v2] = A[v1,v2] + 1;    # v1 -> v2
                    A[v2,v1] = A[v2,v1] + 1;    # v2 -> v1
                    
                    # Update triangle counts
                    t_in[v1] = t_in[v1] - 1;
                    t_in[v2] = t_in[v2] - 1;
                # Update even if we didn't find a match
                t_in[i] = t_in[i] - 1;
        
    while sum(t_out[0:community_threshold]) >= 2 or sum(t_out[community_threshold:n]) >= 2:   # We can still make some triangles
        for i in range(n):
            # We still want to make triangles ACROSS communities
            if t_out[i] > 0:
                    # Find candidate vertices (i.e., i s.t. t_out[i] > 0)
                if i >= community_threshold:    # We're in community 1
                    [v1, v2] = pickTriangle(i, t_out[0:community_threshold]);
                else:                           # We're in community 2
                    [v1, v2] = pickTriangle(i, t_out[community_threshold:n]);
                    v1 = v1 + community_threshold;
                    v2 = v2 + community_threshold;
            
                if v1 != -1: # We were able to find a match
                    # Update the adjacency matrix
                    A[i,v1] = A[i,v1] + 1;      # i -> v1
                    A[v1,i] = A[v1,i] + 1;      # v1 -> i
                    A[i,v2] = A[i,v2] + 1;      # i -> v2
                    A[v2,i] = A[v2,i] + 1;      # v2 -> i
                    A[v1,v2] = A[v1,v2] + 1;    # v1 -> v2
                    A[v2,v1] = A[v2,v1] + 1;    # v2 -> v1
                    
                    # Update triangle counts
                    t_out[v1] = t_out[v1] - 1;
                    t_out[v2] = t_out[v2] - 1;
                # Update even if we didn't find a match
                t_out[i] = t_out[i] - 1;
                
    while sum(t_het[0:community_threshold]) >= 2 or sum(t_het[community_threshold:n]) >= 2:   # We can still make some triangles
        for i in range(n):
            # We still want to make triangles with one corner in each community
            if t_het[i] > 0:
                # Find candidate vertices (i.e., i s.t. t_het[i] > 0)
                v1 = pickNode(i, t_het[0:community_threshold]);
                v2 = pickNode(i, t_het[community_threshold:n]);
            
                if v1 != -1: # We were able to find a match
                    # Update the adjacency matrix
                    A[i,v1] = A[i,v1] + 1;      # i -> v1
                    A[v1,i] = A[v1,i] + 1;      # v1 -> i
                    A[i,v2] = A[i,v2] + 1;      # i -> v2
                    A[v2,i] = A[v2,i] + 1;      # v2 -> i
                    A[v1,v2] = A[v1,v2] + 1;    # v1 -> v2
                    A[v2,v1] = A[v2,v1] + 1;    # v2 -> v1
                    
                    # Update triangle counts
                    t_het[v1] = t_het[v1] - 1;
                    t_het[v2] = t_het[v2] - 1;
                # Update even if we didn't find a match
                t_het[i] = t_het[i] - 1;
    
    # We still want to make edges WITHIN communities
    while sum(s_in[0:community_threshold]) >= 2 or sum(s_in[community_threshold:n]) >= 2:   # We can still make some edges
        for i in range(n):
            # We still want to make triangles with one corner in each community
            if s_in[i] > 0:
                # Pick a node to make an edge with
                if i < community_threshold:    # We're in community 1
                    v1 = pickNode(i, s_in[0:community_threshold]);
                else:                           # We're in community 2
                    v1 = pickNode(i - community_threshold, s_in[community_threshold:n]);
                    v1 = v1 + community_threshold;
        
                if v1 != -1: # We were able to find a match
                    # Update the adjacency matrix
                    A[i,v1] = A[i,v1] + 1;      # i -> v1
                    A[v1,i] = A[v1,i] + 1;      # v1 -> i
                    
                    # Update edge counts
                    s_in[v1] = s_in[v1] - 1;
                # Update even if we didn't find a match
                s_in[i] = s_in[i] - 1;
        
    while sum(s_out[0:community_threshold]) >= 2 or sum(s_out[community_threshold:n]) >= 2:   # We can still make some edges
        for i in range(n):
            # We still want to make triangles with one corner in each community
            if s_out[i] > 0:
                # Pick a node to make an edge with
                if i >= community_threshold:    # We're in community 1
                    v1 = pickNode(i, s_out[0:community_threshold]);
                else:                           # We're in community 2
                    v1 = pickNode(i + community_threshold, s_out[community_threshold:n]);
                    v1 = v1 + community_threshold;
        
                if v1 != -1: # We were able to find a match
                    # Update the adjacency matrix
                    A[i,v1] = A[i,v1] + 1;      # i -> v1
                    A[v1,i] = A[v1,i] + 1;      # v1 -> i
                    
                    # Update edge counts
                    s_out[v1] = s_out[v1] - 1;
                # Update even if we didn't find a match
                s_out[i] = s_out[i] - 1;
    #print 'Out edges done.'
        
    # Make and plot a graph object from the adjacency matrix A
    if show_plot:
        G = nx.from_numpy_matrix(A);
        pos = nx.spring_layout(G);
        nx.draw_networkx_nodes(G,pos,nodelist=range(community_threshold),node_color='r');
        nx.draw_networkx_nodes(G,pos,nodelist=range(community_threshold,n),node_color='b');
        nx.draw_networkx_edges(G,pos)
    
    return A


# Make a bunch of matrices and save them
for k in range(num_networks):
    # Save the network
    A = makeNewman(n, community_threshold, c_in, c_out, k_in, k_out, k_het, False, False)
    fname = 'Newman_Networks/Network_' + str(k+1) + '.txt'
    np.savetxt(fname, A, delimiter=',')
    # Save the community membership of each node
    classes[0:community_threshold] = 1;
    classes[community_threshold:n] = 2;
    fname = 'Newman_Networks/Classes_' + str(k+1) + '.txt'
    np.savetxt(fname, classes, delimiter=',')
    print k
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    