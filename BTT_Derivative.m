function [L2, dL2, dv2] = BTT_Derivative(A, classes, alpha, show_plots)

% Make a graph object from A
G = digraph(A);
n = size(A,1);
% Get the edges. Column 1 is source node, 2 is target node, 3 is weight.
E = table2array(G.Edges);
m = size(E,1)/2;

% Calculate the derivative of BTT(a) w.r.t. a; 1 where there's a
% triangle-completing edge, 0 otherwise.
B = makeBTT(A, 'unif', alpha, 0);
dB = makeBTT(A, 'unif', 10, 0);
dB(dB == 1) = 0;
dB = dB / 10;

% Calculate eigenvalues, in descending order
[V, D, W] = eig(full(B));
[D, I] = sort(diag(D),'descend');
V = V(:,I);
W = W(:,I);

% Find 2nd largest purely real eigenvalue
[minImag, eigInd] = min(abs(imag(D(2:100))));
eigInd = eigInd + 1;
L2 = D(eigInd);

% Find corresponding right & left (resp.) eigenvectors
v2 = V(:,eigInd);
w2 = W(:,eigInd);

dL2 = w2'*dB*v2;
dv2 = v2;

for j = 1:2*m
    if j ~= eigInd
        dv2 = dv2 + W(:,j)'*dB*v2/(D(eigInd) - D(j))*V(:,j);
    end
end

% Sum up the components, based on nodes
class_est = zeros(n,1);
dclass_est = zeros(n,1);

for i = 1:2*m
    % Add this component's value to the target node of this edge
    class_est(E(i,2)) = class_est(E(i,2)) + v2(i);
    dclass_est(E(i,2)) = dclass_est(E(i,2)) + dv2(i);
end

% Plot class estimates as a scatter plot
if isequal(show_plots, 'scatter')
    colours = [classes == 1, zeros(n,1), classes == 2];
    classify(A,B,classes)
    figure(5); clf;
    hold on
    plot(1:n,zeros(n,1),'--k')
    scatter(1:n, class_est, [], colours, '.')
    hold off;
    xlabel('Node')
    ylabel('Class Estimate Value')
    ylim([-max(abs(ylim)) max(abs(ylim))])

% Plot class estimates with error bars corresponding to their alpha
% derivative
elseif isequal(show_plots, 'error')
    colours = [classes == 1, zeros(n,1), classes == 2];
    classify(A,B,classes)
    figure(5); clf;
    hold on
    plot(1:n,zeros(n,1),'--k')
    errorbar(1:n/2, class_est(1:n/2), dclass_est(1:n/2), '.r')
    errorbar(n/2+1:n, class_est(n/2+1:n), dclass_est(n/2+1:n), '.b')
    hold off;
    title('Class estimate and derivative w.r.t. \alpha')
    xlabel('Node')
    ylabel('Class Estimate Value')
    ylim([-max(abs(ylim)) max(abs(ylim))])
elseif isequal(show_plots, 'derivative')
    colours = [classes == 1, zeros(n,1), classes == 2];
    classify(A,B,classes)
    figure(5); clf;
    hold on
    plot(1:n,zeros(n,1),'--k')
    errorbar(1:n/2, class_est(1:n/2), dclass_est(1:n/2).*(dclass_est(1:n/2) < 0), dclass_est(1:n/2).*(dclass_est(1:n/2) > 0), '.r')
    errorbar(n/2+1:n, class_est(n/2+1:n), dclass_est(n/2+1:n).*(dclass_est(n/2+1:n) < 0), dclass_est(n/2+1:n).*(dclass_est(n/2+1:n) > 0), '.b')
    hold off;
    title('Class estimate and derivative w.r.t. \alpha')
    xlabel('Node')
    ylabel('Class Estimate Value')
    ylim([-max(abs(ylim)) max(abs(ylim))])
end

end