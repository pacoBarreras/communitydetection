videoName = 'BTT_Class_SA_Below_1_Norm';
workingDir = 'Plots/video';

if ~exist(workingDir, 'dir')
  mkdir(workingDir);
end

outputVideo = VideoWriter(fullfile(workingDir,[videoName '.avi']));
outputVideo.FrameRate = ceil(num_alpha/30);
open(outputVideo)

% For plotting a circle
theta = 0:0.01:2*pi;
xC = cos(theta);
yC = sin(theta);

% For limits
xmin = min(real(spectra{end}));
xmax = max(real(spectra{end}));
ymin = min(imag(spectra{end}));
ymax = max(imag(spectra{end}));

figure;
for a = 1:num_alpha
    clf; hold on;
    alpha = (a - 1)*alpha_inc + alpha_min;
    
    % For specific eigenvalues
    [~, LMi] = max(abs(spectra{a}(2:end)));
    LM = spectra{a}(LMi+1);
    [~, LRi] = max(real(spectra{a}(2:end)));
    LR = spectra{a}(LRi+1);
    LP = maxk(spectra{a}(imag(spectra{a}) == 0),2);
    LP = LP(2);
    
    % For normalized BTT
    if normalized
        d = sum(A,2);
        frac = d./(d-1);
        frac(isinf(frac)) = 0;
        r = sqrt(mean(frac)/mean(d));
    else % For unnormalized BTT
        r = sqrt(spectra{a}(1));
    end
    
    % Plot eigenvalues, and a circle representing the bulk radius
    scatter(real(spectra{a}),imag(spectra{a}),'.b')
    scatter(real(LM),imag(LM),'or')
    scatter(real(LR),imag(LR),'og')
    scatter(real(LP),imag(LP),'ok')
    plot(r*xC, r*yC, '--r')
    title(['\alpha = ' num2str(alpha)])
    xlim([xmin xmax])
    ylim([ymin ymax])
    
    p = get(gca, 'Position');
    h = axes('Parent', gcf, 'Position', [p(1) + (p(1) + p(3))*0.6, p(2) + (p(2) + p(4))*0.65, p(3)*0.25, p(4)*0.25]);
    hold on;
    plot(h,acc_BTT_LM,'-r')
    plot(h,acc_BTT_LR,'-g')
    plot(h,acc_BTT_LP,'-k')
    plot([a, a],ylim,'-m')
    xlim(h, [1 num_alpha])
    set(h, 'XTick', []);
    hold off;
    
    print([workingDir '/image'],'-djpeg90')
    img = imread(fullfile(workingDir,'image.jpg'));
    writeVideo(outputVideo,img);
    pause(0.01);
end

% Removes image.jpg, since it is no longer needed
delete([workingDir '/image.jpg']);
% Finish making the video by closing the output stream
close(outputVideo);
