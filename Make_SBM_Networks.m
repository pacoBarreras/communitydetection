n = 100;
k = 2;
c_in = 5;
c_out = 2;
num_networks = 1000;

% Folder for saving the created networks
saveDir = 'data/SBM_1000_Below_Threshold';
if ~exist(saveDir, 'dir')
    mkdir(saveDir);
end

disp(['Mixing: ' num2str((c_in - c_out)/2)])
disp(['Threshold: ' num2str(sqrt((c_in + c_out)/2))])
parfor i = 1:num_networks
    A = ER_Adj_Communities(n, k, c_in/(n*k), c_out/(n*k));
    classes = [ones(n,1); 2*ones(n,1)];
    
    % Save the network and classes
    csvwrite([saveDir '/Network_' num2str(i) '.txt'], full(A))
    csvwrite([saveDir '/Classes_' num2str(i) '.txt'], classes)
    disp(i)
end