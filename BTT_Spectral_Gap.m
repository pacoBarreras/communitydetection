% 1. Import network, calculate parameters
% 2. Calculate NBT & BTT
% 3. Classify, measure performance

close all;

% % Get all the files in the random_matrices folder
num_matrices = 100;
networkDir = 'data/SBM_1000_At_Threshold';

% Values of alpha to try
alpha_min = 0.3;
alpha_inc = 0.1;
alpha_max = 3; 
num_alpha = round((alpha_max - alpha_min)/alpha_inc + 1);

% Node-specific types to try
types = {'unif'};
num_Types = size(types,1);

c_in = zeros(num_matrices, 1);
c_out = zeros(num_matrices, 1);
k_in = zeros(num_matrices, 1);
k_mix = zeros(num_matrices, 1);
k_out = zeros(num_matrices, 1);
acc_NBT = zeros(num_matrices, 1);
acc_Lap = zeros(num_matrices, 1);
acc_BTT = zeros(num_Types, num_matrices, num_alpha);
spectral_diff = zeros(num_Types, num_matrices, num_alpha);
lam_1 = zeros(num_Types, num_matrices, num_alpha);
lam_2 = zeros(num_Types, num_matrices, num_alpha);

% For the BTT matrices
bc_in = zeros(num_Types, num_matrices, num_alpha);
bc_out = zeros(num_Types, num_matrices, num_alpha);
bk_in = zeros(num_Types, num_matrices, num_alpha);
bk_mix = zeros(num_Types, num_matrices, num_alpha);
bk_out = zeros(num_Types, num_matrices, num_alpha);

% For eigenvalue validation
conv_NBT = ones(num_matrices,1);
conv_BTT = ones(num_matrices,num_alpha);

% For allowing more iterations of eigenvalue calculation, to ensure
% convergence
opts.maxit = 5000;

for t = 1:num_matrices
    % Import the network
    % [A, classes] = import_Network(files(t));
    A = csvread([networkDir '/Network_' num2str(t) '.txt']);
    n = size(A,1);
    A = sparse((A > 0).*ones(n,n));
    A = A - diag(diag(A));
    classes = csvread([networkDir '/Classes_' num2str(t) '.txt']);
    
    % Make a graph object from A
    G = digraph(A);
    % Get the edges. Column 1 is source node, 2 is target node, 3 is weight.
    E = table2array(G.Edges);
    m = size(E,1);
    
    % Calculate the parameters
    [c_in(t), c_out(t), k_in(t), k_mix(t), k_out(t)] = calculate_Parameters(A, classes);
    
    %%------------------------%%
    %           NBT            %
    %%------------------------%% 
    % Find NBT (final digit 0=unnormalized, 1=normalized, i.e., flow matrix)
    NBT = makeNBT(A, 1, 0);
    
    % Classify & measure the accuracy
    [acc_NBT(t), ~] = classify(A, NBT, classes);
    
    % Calculate third moment
    % third_moment_NBT(t) = 1/m*trace(NBT^3); %mean(NBTval.^3);
    
    %%------------------------%%
    %        Laplacian         %
    %%------------------------%%
%     L = diag(sum(A,1)) - A;
%     [Lapvec, Lapval] = eigs(L, 2, 'LM');
%     
%     class_Lap = (Lapvec(:,2) > 0) + 1;
%     acc_Lap(t) = mean(classes == class_Lap);
%     
%     if acc_Lap(t) < 0.5
%             acc_Lap(t) = 1 - acc_Lap(t);
%     end
    
    %%------------------------%%
    %           BTT            %
    %%------------------------%%
    % Try different values of alpha
    parfor a = 1:num_alpha
        for type = 1:num_Types
            alpha = (a - 1)*alpha_inc + alpha_min;

            % Find BTT weighing according to type
            BTT = makeBTT(A, types{type}, alpha, 0);
            
            %[bc_in(type,t,a), co, ki, km, ko] = calculate_Parameters(B

            % Measure the spectral gap, as a function of alpha
            [vec, val] = eigs(BTT, m, 'LM', opts);
            val = diag(val);
            [minImag, eigInd] = min(abs(imag(val(2:end))));
            spectral_diff(type,t,a) = sqrt(val(1)) - val(eigInd + 1);
            lam_1(type, t, a) = val(1);
            lam_2(type, t, a) = val(eigInd + 1);
            
            % Classify & measure the accuracy
            [acc_BTT(type,t,a), ~] = classify(A, BTT, classes);
        end
    end
    disp(t)
end

c_diff = c_in - c_out;
c_threshold = 2*sqrt((c_in + c_out)/2);
bulk_rad = sqrt( (c_in + c_out)/2 );

mean_NBT = mean(acc_NBT);
mean_Lap = mean(acc_Lap);
[mB, mBi] = max(acc_BTT, [], 2);

colours = hsv(num_Types);
col_mat = hsv(num_matrices);

alphas = alpha_min:alpha_inc:alpha_max;

% Best alpha for each network, based on accuracy
[mv, mi] = max(squeeze(acc_BTT(1,:,:)),[],2);

% Plot the accuracy
figure(1); clf; hold on;
for m = 1:num_matrices
    plot(alphas,squeeze(acc_BTT(1,m,:)),'Color',col_mat(m,:))
end
% Plot points showing the value of alpha that achieved best accuracy
scatter(alphas(mi), diag(squeeze(acc_BTT(1,:,mi))),[],col_mat,'filled')
plot([1, 1],ylim,'--k')
title('Accuracy for each network')
xlabel('\alpha')
ylabel('Accuracy (%)')

% Plot the spectral gap
figure(2); clf; hold on;
for m = 1:num_matrices
%     spectral_gap(imag(spectral_gap) ~= 0) = 0;
    plot(alphas,squeeze(spectral_diff(1,m,:)),'Color',col_mat(m,:))
end
% Plot points showing the value of alpha that achieved best accuracy
scatter(alphas(mi), diag(squeeze(spectral_diff(1,:,mi))),[],col_mat,'filled')
title('Spectral gap for each network')
xlabel('\alpha')
ylabel('Spectral Gap')

% Plot the two largest eigenvalues
figure(3); clf; hold on;
for m = 1:num_matrices
    L1 = abs(squeeze(lam_1(1,m,:)));
    L1(imag(L1) ~= 0) = 0;
    L2 = abs(squeeze(lam_2(1,m,:)));
    L2(imag(L2) ~= 0) = 0;
    plot(alphas,L1,'Color',col_mat(m,:))
    plot(alphas,L2,'Color',col_mat(m,:),'Linestyle','--')
end
scatter(alphas(mi), diag(squeeze(lam_1(1,:,mi))),[],col_mat,'filled')
scatter(alphas(mi), diag(squeeze(lam_2(1,:,mi))),[],col_mat,'filled')
title('Eigenvalues for each network')
xlabel('\alpha')
ylabel('Eigenvalues')

% % Plot accuracy with 95% confidence intervals
% figure(1);
% clf;
% hold on
%     %BTT
%     for type = 1:num_Types
%         mean_BTT = squeeze(mean(acc_BTT(type,:,:),2));
%         plot(alpha_min:alpha_inc:alpha_max,mean_BTT,'Color',colours(type,:))
%     end
%     % Lap
%     plot(alpha_min:alpha_inc:alpha_max,mean_Lap*ones(num_alpha,1),'-g')
%     %NBT
%     plot(alpha_min:alpha_inc:alpha_max,mean_NBT*ones(num_alpha,1),'-k')
% hold off
% xlim([alpha_min alpha_max])
% xlabel('\alpha')
% ylabel('Accuracy')
% types{num_Types+1} = 'Laplacian'; types{num_Types+2} = 'NBT';
% legend(types,'Location','best')
% title(['Accuracy with varying \alpha, ' num2str(num_matrices) ' networks, c_{in} = ' num2str(mean(c_in)) ', c_{out} = ' num2str(mean(c_out))])