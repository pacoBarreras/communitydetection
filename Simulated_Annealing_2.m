function A = Simulated_Annealing_2(n, k, params, weights, max_iters, tol, show_plots)
% --- INPUTS:
% n         - size of each community
% k         - number of communities
% params    - desired network parameters, of the form
%             [c_in, c_out, k_in, k_mix, k_out] (1-by-5)
% weights   - weighting for each parameter above. Higher weight => more
%             important (5-by-1)
%
% --- OUTPUTS:
% A         - adjacency matrix of final network

% params = [5, 1, 0.2, 0.01, 0.01];
% weights = [5, 5, 100, 50, 50]';
% n = 100;
% k = 2;
% max_iters = 10000;

% Probability of connection within community
p_in = params(1)/(n*k);
% Probability of connection accros communities
p_out = params(2)/(n*k);
% To track the number of iterations
iters = 1;

% Initial temperature
T_init = 100;
T = T_init;

% Generate the initial SBM network
A = ER_Adj_Communities(n, k, p_in, p_out);
classes = [ones(n,1); 2*ones(n,1)];
e = Energy(A, classes, params, weights);
eList = zeros(max_iters,1);

% For community colour-coding
if show_plots
    nodeColor = [classes==1, zeros(n*k,1), classes==2];
end

% Perform the simulated annealing until we reach the max number of
% iterations or the energy drops below the tolerance threshold.
while iters < max_iters && e > tol
%     if rand(1) < 1/2
%         % Generate neighbour candidate by picking two edges uniformly at
%         % random, and crossing them.
%         % Pick two different nodes at random, that are connected to at least 1
%         % other node
%         i = randsample(n*k, 1);
%         u = randsample(n*k, 1);
%         while sum(A(i,:)) == 0 || sum(A(u,:)) == 0 || u == i
%             i = randsample(n*k, 1);
%             u = randsample(n*k, 1);
%         end
%         % Pick random node connected to i, and another connected to u.
%         j = randsample(find(A(i,:) > 0),1);
%         v = randsample(find(A(u,:) > 0),1);
%         % Cross the edges: remove (i,j) and (u,v), create (i,v) and (u,j).
%         Anew = A;
%         Anew(i,j) = 0;
%         Anew(j,i) = 0;
%         Anew(u,v) = 0;
%         Anew(v,u) = 0;
%         Anew(i,v) = 1;
%         Anew(v,i) = 1;
%         Anew(u,j) = 1;
%         Anew(j,u) = 1;
%     else
%         rnd_nodes = randsample(n*k,2);
%         i = rnd_nodes(1);
%         j = rnd_nodes(2);
%         Anew = A;
%         Anew(i,j) = xor(Anew(i,j),1);
%         Anew(j,i) = Anew(i,j);
%     end

    % Generate neighbour candidate by rewiring an edge, keeping the same
    % community structure (in-in, in-out, etc).
    % Pick the first node, ensuring it has at least 1 neighbour
    i = randsample(n*k,1);
    while sum(A(i,:)) == 0
        i = randsample(n*k,1);
    end
    nbhd = A(i,:).*(1:n*k);
    % Pick j as a neighbour of i; we will rewire by removing (i,j) and
    % adding (i,u).
    j = randsample(nbhd(nbhd>0),1);
    % Pick u to be in the same community as j. Make sure it's not equal to
    % i, and that the edge does not already exist.
    u = randsample(n,1) + (ceil(j/n) - 1)*n;
    while u == i || A(i,u) > 0
        u = randsample(n,1) + (ceil(j/n) - 1)*n;
    end
    Anew = A;
    Anew(i,j) = 0;
    Anew(j,i) = 0;
    Anew(i,u) = 1;
    Anew(u,i) = 1;
    
    % Check the energy of the neighbour, and determine whether we
    % transition
    eprime = Energy(Anew, classes, params, weights);
    eList(iters) = eprime;
    p = Accept_Prob(e,eprime,T);
    
    if p > rand(1) % Accept the transition
        A = Anew;
        e = eprime;
        if show_plots
            plot(graph(A),'Nodecolor',nodeColor,'Nodelabel',[]);
            title(['Iteration ' num2str(iters) ', Energy ' num2str(e) ', Temp ' num2str(T) ', Accept Prob ' num2str(p)])
            pause(0.01);
        end
    end
    
    T = T_init/iters^2;
    iters = iters + 1;
end

% Plot final state and show final parameters
if show_plots
    plot(graph(A),'layout','force','Nodecolor',nodeColor,'Nodelabel',[]);
    [c_in, c_out, k_in, k_mix, k_out] = calculate_Parameters(A, classes)
    figure;
    plot(eList)
    title('Energy over Time')
end