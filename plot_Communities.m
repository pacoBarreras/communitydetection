function plot_Communities(A, classes)
    n = size(A,1);
    cols = [classes == 1, zeros(n,1), classes == 2];
    figure; clf;
    hold on;
    p = plot(graph(A), 'layout','force','Nodelabel',[],'Nodecolor',cols);
    set(gca,'Visible','off')
    %xlim([min(p.XData)*1.05 max(p.XData)*1.05])
    %ylim([min(p.YData)*1.05 max(p.YData)*1.05])
    set(gca,'LooseInset',get(gca,'TightInset'))
end