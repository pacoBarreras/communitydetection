n = 100;
k = 2;
params = [5, 2, 0.2, 0.01, 0.01];
weights = [1, 1, 500, 500, 500]';
max_iters = 10000;
tol = 0.1;
num_networks = 100;

% Folder for saving the created networks
saveDir = 'data/SA_Networks_Rewire_Below_Threshold';
if ~exist(saveDir, 'dir')
    mkdir(saveDir);
end

% Ensure the parameters selected are valid
if ~check_valid_parameters(params)
    disp('Invalid parameter selection.')
else
    disp(['Mixing: ' num2str((params(1) - params(2))/2)])
    disp(['Threshold: ' num2str(sqrt((params(1) + params(2))/2))])
    parfor i = 1:num_networks
        A = Simulated_Annealing_2(n, k, params, weights, max_iters, tol, false);
        classes = [ones(n,1); 2*ones(n,1)];

        % Save the network and classes
        csvwrite([saveDir '/Network_' num2str(i) '.txt'], full(A))
        csvwrite([saveDir '/Classes_' num2str(i) '.txt'], classes)
        disp(i)
    end
end